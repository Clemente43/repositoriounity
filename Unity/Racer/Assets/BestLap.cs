﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Scriptable object para registrar la vuelta más rapida del circuito
[CreateAssetMenu(fileName = "BestLap", menuName = "BestLap", order=1)]
public class BestLap : ScriptableObject
{
    [SerializeField]
    public float bestLap { get; set; }
}
