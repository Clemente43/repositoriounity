﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[System.Serializable]
public class AxleInfo {
    public WheelCollider leftWheel;
    public WheelCollider rightWheel;
    public bool motor;
    public bool steering;

}
         
public class CarroController : MonoBehaviour {
    public List<AxleInfo> axleInfos; 
    public float maxMotorTorque;
    public float maxSteeringAngle;
    public GameObject turbo;
    public Light luz1;
    public Light luz2;
    public bool lucesOn;
    public bool largasOn;
    private float accTurbo = 0;
    public WheelCollider ruedaR;
    public WheelCollider ruedaL;
    public Light rojaL;
    public Light rojaR;
    public GameObject inicio;
    public GameObject Respawn;
    private GameObject respawn1;
    private GameObject respawn2;
    private GameObject respawn3;
    private GameObject respawn4;
    private GameObject respawn5;
    private float Contador;
    private bool start;
    public Text tiempo;
    public Text BestLapText;
    public Text velText;
    [SerializeField]
    private BestLap bestLap;
    public Image ITurbo;
    public AudioSource fiaun;

    private void Start()
    {
        BestLapText.text = "BEST LAP: " + bestLap.bestLap;
        
        respawn1 = GameObject.Find("punto1");
        respawn2 = GameObject.Find("punto2");
        respawn3 = GameObject.Find("punto3");
        respawn4 = GameObject.Find("punto4");
        respawn5 = GameObject.Find("punto5");
    }
    // finds the corresponding visual wheel
    // correctly applies the transform
    public void ApplyLocalPositionToVisuals(WheelCollider collider)
    {
       
        if (collider.transform.childCount == 0) {
            return;
        }
         
        Transform visualWheel = collider.transform.GetChild(0);
        
        Vector3 position;
        Quaternion rotation;
        collider.GetWorldPose(out position, out rotation);
         
        visualWheel.transform.position = position;
        visualWheel.transform.rotation = rotation;
        
    }
         
    public void FixedUpdate()
    {
        
        float motor = maxMotorTorque * Input.GetAxis("Vertical");
        float steering = maxSteeringAngle * Input.GetAxis("Horizontal");
         
        foreach (AxleInfo axleInfo in axleInfos) {
            if (axleInfo.steering) {
                axleInfo.leftWheel.steerAngle = steering;
                axleInfo.rightWheel.steerAngle = steering;
            }
            if (axleInfo.motor) {
                axleInfo.leftWheel.motorTorque = motor;
                axleInfo.rightWheel.motorTorque = motor;
            }
            ApplyLocalPositionToVisuals(axleInfo.leftWheel);
            ApplyLocalPositionToVisuals(axleInfo.rightWheel);
        }
        
        //intento de derrape
        if (Input.GetKey("space"))
        {
            WheelFrictionCurve sf = ruedaL.sidewaysFriction;
            WheelFrictionCurve sf2 = ruedaR.sidewaysFriction;
            sf2.stiffness = 1;
            sf.stiffness = 1;
            //derrape.SetActive(true);
            //derrape2.SetActive(true);
            accTurbo+=0.3f;
            rojaL.intensity = 20;
            rojaR.intensity = 20;
        }
        else
        {
            WheelFrictionCurve sf = ruedaL.sidewaysFriction;
            WheelFrictionCurve sf2 = ruedaR.sidewaysFriction;
            sf2.stiffness = 5;
            sf.stiffness = 5;
            //derrape.SetActive(false);
            //derrape2.SetActive(false);
            rojaL.intensity = 1;
            rojaR.intensity = 1;
        }
        //barra del turbo
        ITurbo.gameObject.GetComponent<Image>().fillAmount = accTurbo / 100;
        //si apretas la tecla t y tienes turbo acumulado el torque del coche aumentara y hará que el coche corra más y sonará francesco virgolini FIAAAAAAUUUN
        if (Input.GetKey("t")&& accTurbo > 0)
        {
            maxMotorTorque = 2000;
            fiaun.Play();
            accTurbo-=1;
            turbo.SetActive(true);
        }else{
            maxMotorTorque = 1000;
            accTurbo += 0.1f;
            turbo.SetActive(false);
        }
        //si se pulsa la tecla r se respawneara al ultimo punto de control por el que se haya pasado
        if (Input.GetKey("r"))
        {
            this.gameObject.transform.position = Respawn.transform.position;
            this.gameObject.transform.rotation = Respawn.transform.rotation;
            
        }
        //nose porque pero va mal
        //Cambio de luces. Cortas, largas y apagadas
        if (Input.GetKeyDown("l")&&largasOn&&!lucesOn)
        {
            largasOn = false;
            lucesOn = false;
            luz1.gameObject.SetActive(false);
            luz2.gameObject.SetActive(false);
        }
        else
        if (Input.GetKeyDown("l") && !lucesOn && !largasOn)
        {
            luz1.gameObject.SetActive(true);
            luz2.gameObject.SetActive(true);
            lucesOn = true;
            largasOn = false;
            luz1.range = 20;
            luz2.range = 20;
        }
        else
        if (Input.GetKeyDown("l") && lucesOn && !largasOn)
        {
            luz1.gameObject.SetActive(true);
            luz2.gameObject.SetActive(true);
            lucesOn = false;
            largasOn = true;
            luz1.range = 30;
            luz2.range = 30;
        }

        //empieza a contar el tiempo que pasa en tiempo real
        if (start)
        {
            Contador += 1 * Time.deltaTime;
            tiempo.text = Contador.ToString();

        }
        //representacion de la velocidad del coche
        velText.text = Mathf.Round(this.gameObject.GetComponent<Rigidbody>().velocity.magnitude).ToString();
    }
    //si colisiona con el portal se mira el tiempo y si es mas bajo que el que estaba como mejor vuelta se adjudica como mejor vuelta y empieza otra vez la cuenta y la carrera
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "portal")
        {
            this.gameObject.transform.position = inicio.transform.position;
            this.gameObject.transform.rotation = inicio.transform.rotation;
            if (Contador == 0)
            {
                BestLapText.text = "BEST LAP: " + bestLap.bestLap;
            }
            else if (Contador < bestLap.bestLap && bestLap.bestLap != 0 || Contador > bestLap.bestLap && bestLap.bestLap == 0)
            {
                bestLap.bestLap = Contador;
                BestLapText.text = "BEST LAP: " + bestLap.bestLap;
            }
        }
    }
    //se adjudican los checkpoints donde reaparecer si pulsas "r" y se inicia la carrera
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == inicio)
        {
            start = true;
            Contador = 0;
        }
        if (other.gameObject == respawn1)
        {
            Respawn = respawn1;

        }else if (other.gameObject == respawn2)
        {
            Respawn = respawn2;
        }
        else if (other.gameObject == respawn3)
        {
            Respawn = respawn3;
        }
        else if (other.gameObject == respawn4)
        {
            Respawn = respawn4;
        }
        else if (other.gameObject == respawn5)
        {
            Respawn = respawn5;
        }
    }
}