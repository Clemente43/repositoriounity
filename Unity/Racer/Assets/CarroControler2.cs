﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

[System.Serializable]
public class AxleInfo2
{
    public WheelCollider leftWheel;
    public WheelCollider rightWheel;
    public bool motor;
    public bool steering;

}

public class CarroControler2 : MonoBehaviour
{
    public List<AxleInfo2> axleInfos;
    public float maxMotorTorque;
    public float maxSteeringAngle;
    public GameObject turbo;
    public GameObject turbo2;
    public GameObject derrape;
    public GameObject derrape2;
    private float accTurbo = 0;
    public WheelCollider ruedaR;
    public WheelCollider ruedaL;
    public Light rojaL;
    public Light rojaR;
    public GameObject inicio;
    public GameObject Respawn;
    private GameObject respawn1;
    private GameObject respawn2;
    private GameObject respawn3;
    private GameObject respawn4;
    private GameObject respawn5;
    private float Contador;
    private bool start;
    public Text tiempo;
    public Text BestLapText;
    private float bestLap;

    private void Start()
    {
        respawn1 = GameObject.Find("punto1");
        respawn2 = GameObject.Find("punto2");
        respawn3 = GameObject.Find("punto3");
        respawn4 = GameObject.Find("punto4");
        respawn5 = GameObject.Find("punto5");
    }
    // finds the corresponding visual wheel
    // correctly applies the transform
    public void ApplyLocalPositionToVisuals(WheelCollider collider)
    {

        if (collider.transform.childCount == 0)
        {
            return;
        }

        Transform visualWheel = collider.transform.GetChild(0);

        Vector3 position;
        Quaternion rotation;
        collider.GetWorldPose(out position, out rotation);

        visualWheel.transform.position = position;
        visualWheel.transform.rotation = rotation;

    }

    public void FixedUpdate()
    {

        float motor = maxMotorTorque * Input.GetAxis("Vertical");
        float steering = maxSteeringAngle * Input.GetAxis("Horizontal");

        foreach (AxleInfo2 axleInfo in axleInfos)
        {
            if (axleInfo.steering)
            {
                axleInfo.leftWheel.steerAngle = steering;
                axleInfo.rightWheel.steerAngle = steering;
            }
            if (axleInfo.motor)
            {
                axleInfo.leftWheel.motorTorque = motor;
                axleInfo.rightWheel.motorTorque = motor;
            }
            ApplyLocalPositionToVisuals(axleInfo.leftWheel);
            ApplyLocalPositionToVisuals(axleInfo.rightWheel);
        }

        if (Input.GetKey("space"))
        {
            WheelFrictionCurve sf = ruedaL.sidewaysFriction;
            WheelFrictionCurve sf2 = ruedaR.sidewaysFriction;
            //sf2.stiffness = 0;
            //sf.stiffness = 0;
            derrape.SetActive(true);
            derrape2.SetActive(true);
            accTurbo += 0.3f;
            rojaL.intensity = 15;
            rojaR.intensity = 15;
        }
        else
        {
            WheelFrictionCurve sf = ruedaL.sidewaysFriction;
            WheelFrictionCurve sf2 = ruedaR.sidewaysFriction;
            //sf2.stiffness = 5;
            //sf.stiffness = 5;
            derrape.SetActive(false);
            derrape2.SetActive(false);
            rojaL.intensity = 1;
            rojaR.intensity = 1;
        }
        if (Input.GetKey("p"))
        {
            if (accTurbo > 0)
            {
                accTurbo -= 0.5f;
                turbo.SetActive(true);
                turbo2.SetActive(true);
            }
        }
        else
        {
            turbo.SetActive(false);
            turbo2.SetActive(false);
        }

        if (Input.GetKey("r"))
        {
            this.gameObject.transform.position = Respawn.transform.position;
            this.gameObject.transform.rotation = Respawn.transform.rotation;

        }
        if (start)
        {
            Contador += 1 * Time.deltaTime;
            tiempo.text = Contador.ToString();
        }

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "portal")
        {
            this.gameObject.transform.position = inicio.transform.position;
            this.gameObject.transform.rotation = inicio.transform.rotation;
            start = true;
            if (Contador > bestLap)
            {
                bestLap = Contador;
                BestLapText.text += bestLap.ToString();
            }

            Contador = 0;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == respawn1)
        {
            Respawn = respawn1;

        }
        else if (other.gameObject == respawn2)
        {
            Respawn = respawn2;
        }
        else if (other.gameObject == respawn3)
        {
            Respawn = respawn3;
        }
        else if (other.gameObject == respawn4)
        {
            Respawn = respawn4;
        }
        else if (other.gameObject == respawn5)
        {
            Respawn = respawn5;
        }
    }
}
