﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    private bool cambioCamera = true;
    public Camera mainCamera; 
    public CarroController carro; 
    public Camera fpCamera;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void Update ()
	{
		// LERP fa una interpolació lineal. Aixo fara un moviment de camara molt més smooth que no si directament ho posem a la posició del jugador
		mainCamera.transform.position = new Vector3
		(
            //volem la posició no exactament en el jugador, sino que la volem una mica enrere i damunt seu. Si el jugador gira la camara s'hauria de moure respecte al seu nou darrere, orbitant
			Mathf.Lerp(mainCamera.transform.position.x, (carro.transform.position.x - (carro.transform.forward.x * 8)), Time.deltaTime * 5),
			Mathf.Lerp(mainCamera.transform.position.y, (carro.transform.position.y - (carro.transform.forward.y * 8))+5, Time.deltaTime * 10),
			Mathf.Lerp(mainCamera.transform.position.z, (carro.transform.position.z - (carro.transform.forward.z * 8)), Time.deltaTime * 5)
		);
        mainCamera.transform.LookAt(carro.transform);
        
        //cambio de camara a dentro del coche
        if(Input.GetKeyDown("c")&&cambioCamera){
            mainCamera.gameObject.SetActive(false);
            fpCamera.gameObject.SetActive(true);
            cambioCamera=false;
        }else if(Input.GetKeyDown("c")&&!cambioCamera){
            mainCamera.gameObject.SetActive(true);
            fpCamera.gameObject.SetActive(false);
            cambioCamera=true;
        }
            
        
        
    }
}
