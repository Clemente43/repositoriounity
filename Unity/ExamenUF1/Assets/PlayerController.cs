﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class PlayerController : MonoBehaviour
{
    public bool derecha;
    public bool izquierda;
    public bool abajo;
    public bool arriba;
    public int vel=5;
    static GameObject player = null;
    public GameObject[] flechasArray;
    private GameObject newFlecha;
    public static int flechas = 5;
    public static int vidasEnemigo=5;

    // Start is called before the first frame update
    void Start()
    {
        
    }
    void Awake()
    {
        //Patró singleton
        if (player == null)
        {
            //crea el objecte en el primer moment
            player = this.gameObject;
            //per defecte els objectes es destrueixen al carregar una altra escena. 
            //D'aquesta manera no es destrueix al carretgarse
            DontDestroyOnLoad(player);
}
        else
        {
            //si no es el primer objecte creat (perque tornes a l'escena a on esc crea, es destrueix automàticament, d'aquesta forma no téns múltiples instàncies del mateix objecte (singleton)
            Destroy(this.gameObject);
        }


    }
    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Animator>().SetBool("arriba", arriba);
        this.GetComponent<Animator>().SetBool("abajo", abajo);
        this.GetComponent<Animator>().SetBool("derecha", derecha);
        this.GetComponent<Animator>().SetBool("izquierda", izquierda);
        //Ir hacia arriba
            if (Input.GetKey("w"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, vel);
                arriba = true;
                abajo = false;
            }//Ir hacia abajo
            else if (Input.GetKey("s"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -vel);
                arriba = false;
                abajo = true;
            }//Quedarse quieto en el eje y
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);
                arriba = false;
                abajo = false;
            }//Ir hacia la derecha    
            if (Input.GetKey("d"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
                derecha = true;
                izquierda = false;
            }//Ir hacia la izquierda
            else if (Input.GetKey("a"))
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
                derecha = false;
                izquierda = true;
            }//Quedarse quieto en el eje x
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
                derecha = false;
                izquierda = false;
            }
            if(Input.GetKey("p")){
                if(SceneManager.GetActiveScene().name=="SampleScene"){
                SceneManager.LoadScene("SegundaEscena");
                }else{
                    SceneManager.LoadScene("SampleScene");

                }
            }
            
    }
    public void Disparo()
    {
            newFlecha = Instantiate(flechasArray[0]);
            newFlecha.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 5);
            //mueve la posicion a la posicion del player
            newFlecha.transform.position = new Vector2(this.transform.position.x, this.transform.position.y + 1f);
            PlayerController.flechas--;
        }
        
    private void OnCollisionEnter2D(Collision2D other) {
        if(other.gameObject.CompareTag("Enemigo")){
            Destroy(this.gameObject);
            Debug.Log("F");
        }
        
    }
}

