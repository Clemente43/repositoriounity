﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class EnemigoController : MonoBehaviour
{
    public PlayerController player;
    public bool derecha;
    public bool izquierda;
    public bool abajo;
    public bool arriba;
    private float velocidad= 5/2;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        compararPos();
        Invoke("SubirNivel", 5);
    }

    private void compararPos() {
        float distanciaX = player.GetComponent<Transform>().position.x - this.GetComponent<Transform>().position.x;
        float distanciaY = player.GetComponent<Transform>().position.y - this.GetComponent<Transform>().position.y;
        if (Mathf.Abs(distanciaX) > Mathf.Abs(distanciaY))
        {
            if (distanciaX > 0.1f)
            {
                derecha = true;
                izquierda = false;
                abajo = false;
                arriba = false;
            }
            else
            {
                derecha = false;
                izquierda = true;
                abajo = false;
                arriba = false;
            }
        }else if (Mathf.Abs(distanciaX) < Mathf.Abs(distanciaY))
        {
            if (distanciaY > 0.1f)
            {
                derecha = false;
                izquierda = false;
                abajo = false;
                arriba = true;
            }
            else
            {
                derecha = false;
                izquierda = false;
                abajo = true;
                arriba = false;
            }
        }
        if(arriba){
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, velocidad);
        }else 
        if(abajo){
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -velocidad);
        }
        if(derecha){
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(velocidad, this.GetComponent<Rigidbody2D>().velocity.y);
        }else
        if(izquierda){
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(-velocidad, this.GetComponent<Rigidbody2D>().velocity.y);
        }

    }
    public void SubirNivel(){
        Mathf.Pow(velocidad, 1.05f);
    }
    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.name=="flecha"){
            Destroy(this.gameObject);
            //player.vidasEnemigo--;
        }
    }
}
