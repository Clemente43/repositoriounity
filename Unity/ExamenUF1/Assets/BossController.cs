﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour
{
    public bool parriba=true;
    public bool pabajo;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(parriba){
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 5);
        }else if(pabajo){
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -5);
        }
    }
    private void OnCollisionEnter2D(Collision2D other) {
        if(parriba){
            pabajo=true;
            parriba=false;
        }else{
            pabajo=false;
            parriba=true;
        }
    }
}
