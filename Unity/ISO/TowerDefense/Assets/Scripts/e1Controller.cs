﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class e1Controller : MonoBehaviour
{
    GameObject rango;
    GameObject edificio;
    public delegate void idTorre(int ID);
    public event idTorre idTorreEvent;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
            Tilemap tilemap = GameObject.Find("trigger").GetComponent<Tilemap>();
            Vector3Int gridPos = tilemap.WorldToCell(this.transform.position);
            edificio = GameObject.FindWithTag("Tower");   
            //mira si la posicion del grid es la correcta para poder colocar el objeto
            if (tilemap.HasTile(gridPos)){
                //print("PUEDO COLOCAR");
                EdificarController.ahiBien=true;
            }
            else{
               // print("NO PUEDO COLOCAR");
                EdificarController.ahiBien=false;
            }

    }
    //Se coge el id de la torre pulsada
    private void OnMouseDown()
    {
        print("RADIO TORRE");
        idTorreEvent(this.gameObject.GetComponent<Torre>().ID);
        print("RADIO TORRE");
        edificio.gameObject.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject.transform.GetChild(0).gameObject.SetActive(true);
        print("RADIO TORRE");
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (edificio)
        {
            EdificarController.ahiBien = false;
            print("estoy chocando");
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag=="Tower")
        {
            EdificarController.ahiBien = false;
            print("estoy chocando");
        }
    }

}
