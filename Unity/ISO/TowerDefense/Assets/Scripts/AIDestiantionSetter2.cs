﻿using UnityEngine;
using System.Collections;

namespace Pathfinding
{
    /// <summary>
    /// Sets the destination of an AI to the position of a specified object.
    /// This component should be attached to a GameObject together with a movement script such as AIPath, RichAI or AILerp.
    /// This component will then make the AI move towards the <see cref="target"/> set on this component.
    ///
    /// See: <see cref="Pathfinding.IAstarAI.destination"/>
    ///
    /// [Open online documentation to see images]
    /// </summary>
    [UniqueComponent(tag = "ai.destination")]
    [HelpURL("http://arongranberg.com/astar/docs/class_pathfinding_1_1_a_i_destination_setter.php")]
    public class AIDestiantionSetter2 : VersionedMonoBehaviour
    {
        /// <summary>The object that the AI should move to</summary>
        //Targets
        public Transform target;
        public Transform target2;
     
        private bool cambioTarget = false;

        IAstarAI ai;

        /*private void Start(){
			towerController = GameObject.Find("GameControllerPrueba");
		}*/

        void OnEnable()
        {
            ai = GetComponent<IAstarAI>();
            // Update the destination right before searching for a path as well.
            // This is enough in theory, but this script will also update the destination every
            // frame as the destination is used for debugging and may be used for other things by other
            // scripts as well. So it makes sense that it is up to date every frame.
            if (ai != null) ai.onSearchPath += Update;
        }

        void OnDisable()
        {
            if (ai != null) ai.onSearchPath -= Update;
        }

        /// <summary>Updates the AI's destination every frame</summary>
        void Update()
        {

            //Si aun no ha llegado al primer target se mueve hacia el, si ya ha llegado se mueve hacia el segundo target
            if (!cambioTarget)
                {
                    if (target != null && ai != null) ai.destination = target.position;
                    if (((this.transform.position.x - target.position.x) <= 1f && (this.transform.position.x - target.position.x) >= -1f) && ((this.transform.position.y - target.position.y) <= 1f && (this.transform.position.y - target.position.y) >= -1f))
                    {
                        cambioTarget = true;
                    }
                }
            else
            {
                if (target2 != null && ai != null)
                {
                    ai.destination = target2.position;
                }
            }
            


        }
     
    }
}

