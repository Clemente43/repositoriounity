﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModoBotones : MonoBehaviour
{
    private int towerSelected;
    public GameObject gameControllerPrueba;
    public GameObject e1Controller;
    public GameObject torreSelected;
    private void Awake()
    {
        //asignar el gameController a una variable, donde estaran todas las torres añadidas en la lista de torres

        gameControllerPrueba = GameObject.Find("GameControllerPrueba");
        //hacemos lo mismo con el e1controller
        e1Controller = GameObject.FindWithTag("Tower");
    }
    // Start is called before the first frame update
    void Start()
    {
        //e1Controller.GetComponent<e1Controller>().idTorreEvent += asignarId;
    }

    // Update is called once per frame
    void Update()
    {

    }
    /*public void OnMouseDown()
    {
        
    }*/
    public void asignarId(int torreID)
    {
        towerSelected = torreID;

        foreach (GameObject torre in gameControllerPrueba.GetComponent<GameControllerPrueba>().torres)
        {
            if (torre.GetComponent<Torre>().ID == torreID)
            {
                torreSelected = torre;
                break;
            }
        }
    }
    public void FirstMode()
    {
        if (torreSelected != null)
        {
            //cambiar parametro
            torreSelected.GetComponent<Torre>().FirstMode();
        }
    }
    public void LastMode()
    {
        if (torreSelected != null)
        {
            //cambiar parametro
            torreSelected.GetComponent<Torre>().LastMode();
        }
    }
    public void StrongestMode()
    {
        if (torreSelected != null)
        {
            //cambiar parametro
            torreSelected.GetComponent<Torre>().StrongestMode();
        }
    }
    public void NearbyMode()
    {
        if (torreSelected != null)
        {
            //cambiar parametro
            torreSelected.GetComponent<Torre>().NearbyMode();
        }
    }
    public void LevelUp()
    {
        if (torreSelected != null)
        {
            //cambiar parametro
            torreSelected.GetComponent<Torre>().LevelUp();
        }
    }
}
