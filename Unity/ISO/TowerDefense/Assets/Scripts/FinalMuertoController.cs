﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
/// <summary>
/// Este componente cuenta y hace saltar el evento de que un enemigo a llegado al final.
/// </summary>
public class FinalMuertoController : MonoBehaviour
{
    public delegate void enemyArrived(GameObject enemy);
    public event enemyArrived enemyArrivedEvent;
    public delegate void bajarVida();
    public event bajarVida eventBajarVida;
    public int contador = 0;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {

            Debug.Log("SaltaTrigger ENEMIGO MUERTO");
            enemyArrivedEvent(collision.gameObject);
            BarraVida.vida -= collision.gameObject.GetComponent<Enemigo>().atack;
            if (BarraVida.vida < 0)
            {
                BarraVida.vida = 0;
                SceneManager.LoadScene("Perder");
            }
            eventBajarVida();
            contador++;
            Debug.Log(contador); 
        }
    }
}
