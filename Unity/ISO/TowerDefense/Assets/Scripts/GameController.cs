﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    // Start is called before the first frame update
    public int contadorEnemigos = 1;
    public float timeSpawn=0.1f;
    public GameObject[] enemigos;
   // public GameObject[] torres;
    public List<GameObject> torres = new List<GameObject>();
    private GameObject enemy;
    void Start()
    {
        InvokeRepeating("instanciarEnemigo", 0, timeSpawn);
        InvokeRepeating("subTowers", 0, 0.2f);
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void instanciarEnemigo()
    {
        //tengo que cambiar muchas cosas aqui, muchisimas
        enemy = enemigos[0];
        Instantiate(enemy);
        enemy.gameObject.name="diamantes"+contadorEnemigos;
        enemy.transform.position = this.transform.position;
        enemy.GetComponent<Enemigo>().ID = contadorEnemigos;
        contadorEnemigos++;
    }
    void subTowers()
    {
        foreach (GameObject tower in torres)
        {
            //Subscribes todas las torres instanciadas al evento que salta cuando proyectil reenvia a una torre basica a la clase padre torre que lo reenvia aqui, es decir Proyectil->Torre Hija -> Torre Padre ->GameController
            tower.GetComponent<Torre>().enemyErasedTowerEvent += Delete;
        }
    }
    void Delete(GameObject tarjet)
    {
       // Debug.Log("Dentro de delete");
        foreach (GameObject tower in torres)
        {
            if (tower.GetComponent<Torre>().tarjets.Contains(tarjet))
            {

                if (tower.GetComponent<Torre>().tarjet.GetComponent<Enemigo>().ID == tarjet.GetComponent<Enemigo>().ID)
                {
                    tower.GetComponent<Torre>().tarjets.Remove(tarjet);
                    tower.GetComponent<Torre>().NextTarget();
                }
                else
                    tower.GetComponent<Torre>().tarjets.Remove(tarjet);
            }
            // tower.GetComponent<Torre>().enemyErasedTowerEvent += Delete;
        }
        Destroy(tarjet);
    }

}
