﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarraTienda : MonoBehaviour
{
    public static float dinero = 10f;
    private Transform bar;
    private float Inicial;
    public GameObject col3;
    public GameObject col4;
    public GameObject col5;
    public GameObject col6;
    // Start is called before the first frame update
    void Start()
    {
        //FinalMuerto.GetComponent<FinalMuertoController>().eventBajarVida += PonerMedida;
        Inicial = 10f;
        bar = transform.Find("Barra2");
        InvokeRepeating("PonerMedida", 0f, 1f);
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void PonerMedida()
    {
        dinero += 0.25f;
        if (dinero >= 10f) 
        {
            dinero = 10f;
        }
        float nivel = ((dinero * 100) / this.Inicial) * 0.01f;
        bar.localScale = new Vector3(nivel, 1f, bar.localScale.z);
        if (dinero>=3)
        {
            col3.SetActive(false);
            if (dinero >= 4)
            {
                col4.SetActive(false);
                if (dinero >= 5)
                {
                    col5.SetActive(false);
                    if (dinero >= 6)
                    {
                        col6.SetActive(false);
                    }
                    else
                    {
                        col6.SetActive(true);
                    }
                }
                else
                {
                    col5.SetActive(true);
                    col6.SetActive(true);
                }
            }
            else
            {
                col4.SetActive(true);
                col5.SetActive(true);
                col6.SetActive(true);
            }
        }
        else
        {
            col3.SetActive(true);
            col4.SetActive(true);
            col5.SetActive(true);
            col6.SetActive(true);
        }      
    }
}
