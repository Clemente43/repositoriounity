﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorreInfernal : Torre
{
    public GameObject proyectil;
    private GameObject newproyectil;

    private float prevTime=0;
    private float currTime;
    private float exponencial=1;

    void Awake()
    {
        base.money=1000;
        base.position = base.transform.position; // DE MOMENTO
        base.maxLife = 120;
        base.currentLife = base.maxLife;
        base.range = 18;
        base.atack = 5;
        base.speedAtack = 0.2f;
        base.splash = false;
        base.mode = Mode.Strongest;
        base.speedProjectile = 1.5f;

        this.GetComponent<CircleCollider2D>().radius = range;
        Debug.Log(base.speedAtack);
        InvokeRepeating("atackAction", 0.1f, base.speedAtack);

    }


    public override void atackAction()
    {
        if (base.tarjet != null)
        {
            
            newproyectil = Instantiate(proyectil);
           // currTime=Time.time;
           // print(currTime-prevTime);
           // prevTime=currTime;

            newproyectil.AddComponent(typeof(Proyectil));
            newproyectil.GetComponent<Proyectil>().atack = Mathf.Pow(this.atack,exponencial);
            
            newproyectil.transform.position = this.transform.position;
            newproyectil.GetComponent<Proyectil>().splash = base.splash;
            newproyectil.GetComponent<Proyectil>().tarjet = base.tarjet;
            newproyectil.GetComponent<Proyectil>().torrePadre = this.gameObject;
            newproyectil.GetComponent<Proyectil>().sp = base.speedProjectile;
            //Esto se subscribe al evento de proyectil que se activa cuando la vida del enemigo es 0 o menos, y lo mandas a la funcion newDelete de la clase padre Torre
            //Proyectil->Torre Hija -> Torre Padre ->GameController
            newproyectil.GetComponent<Proyectil>().enemyErasedEvent += base.newDelete;
            newproyectil.GetComponent<Proyectil>().enemyErasedEvent += this.deleteInferno;
            exponencial++;
            //print(newproyectil.GetComponent<Proyectil>().tarjet.gameObject.GetComponent<Enemigo>().ID);
            //Instanciar Proyectil o el ataque de la torre en si, teniendo como objetivo el tarjet. Habra que tener en cuenta que solo pueda explotar si la colision del proyectil coinicde con el tarjet id, no con el TAG
        }
        else
        {
            Debug.Log("el tarjet es nulo");
            base.NextTarget();
        }

       
    }
        public void deleteInferno(GameObject deleted){
            this.exponencial=1;
        }
}
