﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Clase de la cual heredan todas las torres
/// </summary>
public abstract class Torre : MonoBehaviour
{
    // Start is called before the first frame update
    public Vector2 position;
    int lvl = 1;
    public int ID;//Este ID lo asigna el GameControllerPrueba
    public float money; //No se usa actualmente
    public float maxLife;
    public float currentLife;
    public float range; //Este valor se asigna como el radio del CircleCollider  
    public float atack;
    public bool splash;//No implementado
    public float speedAtack;
    public float speedProjectile;

    public Mode mode;
    /// <summary>
    /// Objetivo actual de la torre
    /// </summary>
    public GameObject tarjet;
    //delegado para el tarjet

    public delegate void enemyErasedTower(GameObject enemyToDelete);
    public event enemyErasedTower enemyErasedTowerEvent;



    /// <summary>
    /// Contiene los distintos metodos de seleccionar el objetivo
    /// </summary>
    public enum Mode { First, Last, Nearby, Strongest, Area };
    /// <summary>
    /// Lista que contiene los  a los que puede disparar la torre
    /// </summary>
    public List<GameObject> tarjets = new List<GameObject>();


    /// <summary>
    /// Metodo que comparten todas las torres de defensa
    /// </summary>
    public abstract void atackAction();



    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            tarjets.Add(other.gameObject);
            if (this.mode == Mode.Strongest)
            {

                Debug.Log(other.gameObject);
                Debug.Log(tarjet);
                if (tarjet != null)
                {
                    if (other.gameObject.GetComponent<Enemigo>().maxLife > tarjet.GetComponent<Enemigo>().maxLife)
                    {
                        tarjet = other.gameObject;
                    }
                }
                else tarjet = other.gameObject;
            }

            if (this.mode == Mode.Last)
            {
                LastMode();
            }

            if (tarjets.Count == 1)
            {
                tarjet = other.gameObject;
            }
        }

    }

    private void OnTriggerExit2D(Collider2D other)
    {

        if (this.mode != Mode.Area)
        {

            if (tarjets.Contains(other.gameObject))
            {
                tarjets.Remove(other.gameObject);
                if (tarjets.Count == 0)
                {
                    tarjet = null;
                }
                else
                {
                    NextTarget();
                }
            }
        }
    }

    /// <summary>
    /// Este modo hace que la torre dispare al primer enemigo que entra al CircleCollider
    /// </summary>
    public void FirstMode()
    {
        this.mode = Mode.First;
        if (tarjets.Count != 0)
        {
            tarjet = tarjets[0];
        }
        else tarjet = null;
    }
    /// <summary>
    /// Este modo hace que la torre dispare al ultimo enemigo que entra al CircleCollider
    /// </summary>
    public void LastMode()
    {
        this.mode = Mode.Last;
        if (tarjets.Count != 0)
        {
            tarjet = tarjets[tarjets.Count - 1];
        }
        else tarjet = null;
    }
    /// <summary>
    /// Este modo hace que la torre dispare al enemigo que tiene la salud maxima más grande.
    /// </summary>
    public void StrongestMode()
    {
        this.mode = Mode.Strongest;
        if (tarjets.Count != 0)
        {
            if (tarjet == null) tarjet = tarjets[0];
            GameObject temporal = tarjets[0];
            foreach (GameObject enm in tarjets)
            {
                
                if (enm.GetComponent<Enemigo>().maxLife > temporal.GetComponent<Enemigo>().maxLife)
                {
                    temporal = enm;
                }
            }
            tarjet = temporal;
        }
        else tarjet = null;
    }
    /// <summary>
    /// Este modo hace que la torre dispare al enemico que tiene mas cercano, pendiente de testeo
    /// </summary>
    public void NearbyMode()
    {
        this.mode = Mode.Nearby;
        if (tarjets.Count != 0)
        {

            if (tarjets.Count == 1)
            {
                tarjet = tarjets[0];
            }
            else
            {
                GameObject temporal = tarjets[0];

                foreach (GameObject enm in tarjets)
                {
                    if (Vector2.SqrMagnitude(temporal.transform.position) < Vector2.SqrMagnitude(enm.transform.position))
                    {
                        Debug.Log(tarjet.GetComponent<Enemigo>().ID);
                        Debug.Log(temporal.GetComponent<Enemigo>().ID);
                        temporal = enm;
                    }
                }
                Debug.Log(tarjet.GetComponent<Enemigo>().ID);
                Debug.Log(temporal.GetComponent<Enemigo>().ID);
                tarjet = temporal;

            }


        }
        else tarjet = null;
    }
    //Meter click derecho + desplegable para cambiar el modo   

    /// <summary>
    /// Evento que salta cuando una torre instanciada reenvia la informacion de un proyectil     
    /// Proyectil->Torre Hija -> Torre Padre ->GameController </summary>
    /// <param name="tarjet"></param>
    public void newDelete(GameObject tarjet)
    {
        enemyErasedTowerEvent(tarjet);



    }


    /// <summary>
    /// Este metodo busca el siguiente objetivo segun el modo actual que este configurado en la variable mode del tipo Mode, que es un Enum
    /// </summary>
    public void NextTarget()
    {
        if (this.mode == Mode.First)
        {
            FirstMode();
        }
        if (this.mode == Mode.Last)
        {
            LastMode();
        }
        if (this.mode == Mode.Strongest)
        {
            StrongestMode();
        }
        if (this.mode == Mode.Nearby)
        {
            NearbyMode();
        }
    }
    /// <summary>
    /// Funcion que sube de nivel, hay que hacer una condicion extra que sea el precio a pagar para poder subirlo de nivel
    /// </summary>
    public void LevelUp()
    {
        if (lvl < 4)
        {
            maxLife = maxLife * 1.2f;
            currentLife = currentLife * 1.2f;
            range = range * 1.1f;
            this.GetComponent<CircleCollider2D>().radius = range;
            atack = atack * 1.1f;

        }
    }

    void FixedUpdate()
    {
        if (this.mode == Mode.Nearby)
        {
            NearbyMode();
        }
        if (tarjet == null)
        {
            NextTarget();
        }
    }


}
