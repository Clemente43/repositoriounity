﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public abstract class Enemigo : MonoBehaviour
{
    public int ID;
    public float maxLife;
    public float currentLife;
    public float atack;
    public float velocity;
    public float velocityAtack;
    public GameObject targetTower = null;

    public delegate void towerErased(GameObject tower);
    public event towerErased towerErasedEvent;

    public void actionAtack()
    {
        targetTower.GetComponentInChildren<Torre>().currentLife -= this.atack;
        Debug.Log(targetTower.GetComponentInChildren<Torre>().currentLife);
        if (targetTower.GetComponentInChildren<Torre>().currentLife <= 0)
        {
            Debug.Log("entra en el if del evento");
            towerErasedEvent(targetTower);
        }
    }

    public void checkTarget()
    {
        if (this.GetComponent<AIDestinationSetter>() == null)
        {
           // Debug.Log("no contiene AIDestinationSetter");
        }
        else
            if (this.GetComponent<AIDestinationSetter>().targetTower != null)
            {
                 targetTower = this.GetComponent<AIDestinationSetter>().targetTower;
                 actionAtack();
             }

    }
    public void StartEnemigo()
    {
        // Debug.Log(this.velocityAtack);
        InvokeRepeating("checkTarget", 0.2f, this.velocityAtack);

    }



}

