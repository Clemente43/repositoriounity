﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class EdificarController : MonoBehaviour
{
    GameObject edificio;
    public Tilemap tilemap;
    public Tilemap trigger;
    public GameObject Edificio1;
    GameObject newEdificio1;
    public static bool ahiBien=false;
    public delegate void reiniciarTilemap();
    public event reiniciarTilemap eventReiniciarTilemap;

    private void OnMouseDown() {
        newEdificio1 = Instantiate(Edificio1);
        newEdificio1.transform.position = new Vector2(Input.mousePosition.x,Input.mousePosition.y);
}
 private void Update() {
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 inputmouse = Input.mousePosition;
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(inputmouse);
            Vector3Int gridPos = tilemap.WorldToCell(mousePos);

            if (tilemap.HasTile(gridPos))
                print("Casella " + gridPos);
        }
        edificio = GameObject.FindWithTag("Tower");
    }
private void OnMouseEnter()
    {

        print("cogidon");

    }

    private void OnMouseExit()
    {
        print("colocasion");
    }

    private void OnMouseDrag()
    {
        newEdificio1.transform.position = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
        
    }

    private void OnMouseUp(){
        print("superbetx");
        if (!ahiBien)
        {
            Destroy(newEdificio1.gameObject);
        }
        else
        {
            newEdificio1.gameObject.GetComponentInChildren<BoxCollider2D>().isTrigger = false;
            eventReiniciarTilemap();
            BarraTienda.dinero -= 3;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Tower")
        {
            ahiBien = false;
            print("estoy chocando");
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (edificio)
        {
            EdificarController.ahiBien = false;
            print("estoy chocando");
        }
    }
}
