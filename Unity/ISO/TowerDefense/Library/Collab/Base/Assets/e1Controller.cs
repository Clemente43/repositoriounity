﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class e1Controller : MonoBehaviour
{
    GameObject rango;
    GameObject edificio;
    public delegate void idTorre(int ID);
    public event idTorre idTorreEvent;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
            Tilemap tilemap = GameObject.Find("trigger").GetComponent<Tilemap>();
            Vector3Int gridPos = tilemap.WorldToCell(this.transform.position);
            edificio = GameObject.FindGameObjectWithTag("Tower");   

            if (tilemap.HasTile(gridPos)){
                print("PUEDO COLOCAR");
                EdificarController.ahiBien=true;
            }
            else{
                print("NO PUEDO COLOCAR");
                EdificarController.ahiBien=false;
            }

    }
    private void OnMouseDown()
    {
        idTorreEvent(this.gameObject.GetComponent<Torre>().ID);

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (edificio)
        {
            EdificarController.ahiBien = false;
            print("estoy chocando");
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag=="Tower")
        {
            EdificarController.ahiBien = false;
            print("estoy chocando");
        }
    }

}
