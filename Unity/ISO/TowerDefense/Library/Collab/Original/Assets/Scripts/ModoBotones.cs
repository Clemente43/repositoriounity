﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModoBotones : MonoBehaviour
{
    private int towerSelected;
    public GameObject gameControllerPrueba;
    public GameObject e1Controller;
    private void Awake()
    {
        //asignar el gameController a una variable, donde estaran todas las torres añadidas en la lista de torres

        gameControllerPrueba = GameObject.Find("GameControllerPrueba");
        //hacemos lo mismo con el e1controller
        e1Controller = GameObject.Find("e1Controller");
    }
    // Start is called before the first frame update
    void Start()
    {
        e1Controller.GetComponent<e1Controller>().idTorreEvent += asignarId;

    }

    // Update is called once per frame
    void Update()
    {
       
    }

    private void asignarId(int torreID)
    {
        towerSelected = torreID;
    }

    private void FirstMode()
    {
        if (towerSelected != null)
        {
            //cambiar parametro
        }
    }
    private void LastMode()
    {
        if (towerSelected != null)
        {
            //cambiar parametro
        }
    }
    private void StrongestMode()
    {
        if (towerSelected != null)
        {
            //cambiar parametro
        }
    }
    private void NearbyMode()
    {
        if (towerSelected != null)
        {
            //cambiar parametro
        }
    }
}
