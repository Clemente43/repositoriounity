﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class EdificarController : MonoBehaviour
{
    GameObject edificio;
    public Tilemap tilemap;
    public Tilemap trigger;
    public GameObject Edificio1;
    GameObject newEdificio1;
    public static bool ahiBien=false;
    public delegate void reiniciarTilemap();
    public event reiniciarTilemap eventReiniciarTilemap;

    //clicas en el cuadrado se instancia el prefab correspondiente en la posicion del mouse
    private void OnMouseDown() {
        newEdificio1 = Instantiate(Edificio1);
        newEdificio1.transform.position = new Vector2(Input.mousePosition.x,Input.mousePosition.y);
}
 private void Update() {
        //miras donde clicas en el tilemap. Es el mouseController
        if (Input.GetMouseButtonDown(0))
        {
            Vector2 inputmouse = Input.mousePosition;
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(inputmouse);
            Vector3Int gridPos = tilemap.WorldToCell(mousePos);

            if (tilemap.HasTile(gridPos))
                print("Casella " + gridPos);
        }
        edificio = GameObject.FindWithTag("Tower");
    }
private void OnMouseEnter()
    {

        print("cogidon");

    }

    private void OnMouseExit()
    {
        print("colocasion");
    }
    //funcion para arrastrar el objeto previamente instanciado en el OnMouseDown
    private void OnMouseDrag()
    {
        newEdificio1.transform.position = new Vector2(Camera.main.ScreenToWorldPoint(Input.mousePosition).x, Camera.main.ScreenToWorldPoint(Input.mousePosition).y);
        
    }
    //cuando sueltas el click del raton mira si se puede colocar en ese posicion y si puede se le desactiva el trigger para que tenga colision, se reinica el tilemap para el A* y se le resta el dinero
    //si no esta bien puesto se destruye. La comprobacion de ahiBien esta en e1Controller
    private void OnMouseUp()
    {
        print("superbetx");
        if (!ahiBien)
        {
            Destroy(newEdificio1.gameObject);
        }
        else
        {
            newEdificio1.gameObject.GetComponentInChildren<BoxCollider2D>().isTrigger = false;
            eventReiniciarTilemap();
            BarraTienda.dinero -= 3;
        }
    }
        //esto no va bien
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Tower")
        {
            ahiBien = false;
            print("estoy chocando");
        }
    }
        //y esto tampoco
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (edificio)
        {
            EdificarController.ahiBien = false;
            print("estoy chocando");
        }
    }
}
