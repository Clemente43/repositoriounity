﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //Bools animator
    public bool abajo = false;
    public bool derecha = false;
    public bool izquierda = false;
    public bool cubrir = false;
    public bool parriba = false;
    public bool pabajo = false;
    public bool combo1 = false;
    public bool combo12 = false;
    public bool combo13 = false;
    public bool combo14 = false;
    //public bool combo2 = false;
    //public bool combo21 = false;


    //public bool puedeAgarre = false
    //indica si se puede hacer el combo
    public bool puedeCombo = true;
    //indica si puede disparar
    public bool puedeDisparar;
    //enemigo en la izquierda
    public bool enemigoIzq;
    //enemigo en la derecha
    public bool enemigoDer;
    //si ha girado
    public bool haGirado = false;
    //velocidad
    public float vel;
    //salto
    public int salto;
    //vida
    public int vida = 100;
    //delegado y evento de recibir daño
    public delegate void hurt();
    public event hurt eventHurt;

    //enemigo
    public GameObject enemigo;
    //disparo
    public GameObject disparo;
    //disparo creado
    private GameObject newDisparo;
    //cuenta para poder disparar otra vez
    private int canShoot = 50;
    //estado del combo
    public int status = 0;

    //public float cargarDisparo = 1f;
    //public bool cargarFull;
    //corutina
    Coroutine lastcoroutine = null;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        //Bools animator
        this.GetComponent<Animator>().SetBool("derecha", derecha);
        this.GetComponent<Animator>().SetBool("izquierda", izquierda);
        this.GetComponent<Animator>().SetBool("abajo", abajo);
        this.GetComponent<Animator>().SetBool("cubrir", cubrir);
        this.GetComponent<Animator>().SetBool("parriba", parriba);
        this.GetComponent<Animator>().SetBool("pabajo", pabajo);

        //activar y desactivar el collider para que se ajuste al personaje
        if (this.GetComponent<PolygonCollider2D>().enabled) {
            Destroy(this.GetComponent<PolygonCollider2D>());
        }
        this.gameObject.AddComponent<PolygonCollider2D>();
        this.GetComponent<PolygonCollider2D>().isTrigger = true;


        //Flip Personaje
        if (this.GetComponent<Transform>().position.x < enemigo.gameObject.GetComponent<Transform>().position.x && !haGirado) {
            enemigoDer = false;
            enemigoIzq = true;
            this.GetComponent<Transform>().Rotate(0, 180, 0);
            haGirado = true;

        }
        else if (this.GetComponent<Transform>().position.x > enemigo.gameObject.GetComponent<Transform>().position.x && haGirado) {
            enemigoIzq = false;
            enemigoDer = true;
            this.GetComponent<Transform>().Rotate(0, 180, 0);
            haGirado = false;

        }

        //Ir hacia la derecha    
        if (Input.GetKey("d"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
            derecha = true;

        }
        //Ir hacia la izquierda
        else if (Input.GetKey("a"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
            izquierda = true;
        }
        //Quedarse quieto
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
            izquierda = false;
            derecha = false;
            abajo = false;

        }
        //Agacharse
        if (Input.GetKey("s"))
        {
            abajo = true;

        }
        //Salto
        else if (Input.GetKeyDown("w"))
        {
            this.GetComponent<Rigidbody2D>().AddForce(Vector2.up * salto, ForceMode2D.Impulse);
            parriba = true;
        }
        else
        {
            abajo = false;
        }
        //Comprovar si cae o si sube
        if (this.GetComponent<Rigidbody2D>().velocity.y < 0)
        {
            pabajo = true;
            parriba = false;
        }
        else
        {
            pabajo = false;

        }
        //Cubrirse
        if (Input.GetKey("p"))
        {
            cubrir = true;
        }
        else
        {
            cubrir = false;

        }
        //Pegar Combo1
        if (Input.GetKeyDown("o"))
        {
            switch (status)
            {
                case 0:
                    print("polla1");
                    //this.GetComponent<Animator>().SetTrigger("combo1");
                    this.GetComponent<Animator>().Play("golpe1Izquierda");
                    status = 1;
                    if (lastcoroutine != null)
                        StopCoroutine(lastcoroutine);
                    lastcoroutine = StartCoroutine(Combo1());


                    break;
                case 1:

                    if (puedeCombo) {
                        print("polla2");
                        //this.GetComponent<Animator>().SetTrigger("combo12");
                        this.GetComponent<Animator>().Play("golpe2Izquierda");
                        status = 2;
                        StopCoroutine(lastcoroutine);
                        lastcoroutine = StartCoroutine(Combo1());

                    } else {
                        status = 0;
                    }

                    break;
                case 2:
                    if (puedeCombo) {
                        print("polla3");
                        //this.GetComponent<Animator>().SetTrigger("combo13");
                        this.GetComponent<Animator>().Play("golpe3Izquierda");
                        status = 3;
                        StopCoroutine(lastcoroutine);
                        lastcoroutine = StartCoroutine(Combo1());
                    } else {
                        status = 0;
                    }

                    break;
                case 3:

                    if (puedeCombo) {
                        print("polla4");
                        //this.GetComponent<Animator>().SetTrigger("combo14");
                        this.GetComponent<Animator>().Play("golpe4Izquierda");
                        if (enemigoDer)
                        {
                            this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, 0);
                        }
                        else
                        {
                            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, 0);

                        }
                        StopCoroutine(lastcoroutine);
                        lastcoroutine = StartCoroutine(Combo1());
                        status = 0;
                    } else {
                        status = 0;
                    }

                    break;

            }

        }



        /*if (Input.GetKey("u")&&puedeAgarre)
        {
            agarre();
        }*/

        //disparar
        if (Input.GetKey("y") && canShoot >= 50) {
            Disparo();

            /*cargarDisparo-=0.1f;
            if(cargarDisparo<=0){
                cargarDisparo=0f;
            }
        }else{
            if(cargarDisparo<1f){
                Disparo();
            }*/
        } else {
            canShoot++;
        }
        //vistoria player1
        if (vida==0)
        {
            Application.LoadLevel("Player1_Win");
        }
    }
    //instanciacion del disparo y su movimiento inicial
    private void Disparo()
    {
            newDisparo = Instantiate(disparo);
            if(enemigoDer){

                newDisparo.gameObject.GetComponent<SpriteRenderer>().flipX=false;
                newDisparo.transform.position = new Vector2(this.transform.position.x-2, this.transform.position.y);
                newDisparo.GetComponent<Rigidbody2D>().velocity = new Vector2(-5, 7);
                newDisparo.GetComponent<Transform>().Rotate(0, 0, -20);
                StartCoroutine(ZigZag(newDisparo));

            }else if(enemigoIzq){
                newDisparo.gameObject.GetComponent<SpriteRenderer>().flipX=true;
                newDisparo.transform.position = new Vector2(this.transform.position.x+2, this.transform.position.y);
                newDisparo.GetComponent<Rigidbody2D>().velocity = new Vector2(5, 7);
                newDisparo.GetComponent<Transform>().Rotate(0, 0, 20);
                StartCoroutine(ZigZag(newDisparo));
            }
            canShoot = 0;           

    }
    //movimiento del disparo
    IEnumerator ZigZag(GameObject nd)
    {
        while(!nd.GetComponent<DisparoController>().haChocau){
            yield return new WaitForSeconds(0.5f);
            nd.GetComponent<Rigidbody2D>().velocity = new Vector2(nd.gameObject.GetComponent<Rigidbody2D>().velocity.x, nd.gameObject.GetComponent<Rigidbody2D>().velocity.y*-1);
            nd.GetComponent<Transform>().Rotate(0,0, nd.gameObject.GetComponent<Transform>().rotation.z*-1);
            //tendria que cambiar pero no cambia
            }
    }
    //Tiempo minimo y maximo para hacer el combo de golpes
    IEnumerator Combo1()
    {
        // ESPERA durante un segundo. el resto del codigo sigue ejecutandose.
        yield return new WaitForSeconds(0.3f);
        puedeCombo=true;
        yield return new WaitForSeconds(1);
        puedeCombo=false;
    }

    /*private void agarre()
    {
            if (enemigoDer)
            {
                print("agarre");
                enemigo.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 15, ForceMode2D.Impulse);
                //enemigo.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(-5, 2));
            }
            else
            {
                enemigo.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.right * salto, ForceMode2D.Impulse);

            }
    }*/

    //colisiones con los proyectiles y golpes
    private void OnTriggerEnter2D(Collider2D other) {
            if(other.tag=="golpe1"&&!cubrir){
                this.GetComponent<Animator>().SetTrigger("pillar");
                vida -=2;
                if(vida<0){
                    vida=0;
                }
                if(eventHurt!=null)
                eventHurt();
            }
            if(other.tag=="golpe2"&&!cubrir){
                this.GetComponent<Animator>().SetTrigger("pillar");
                vida -= 3;
                if(vida<0){
                    vida=0;
                }
                if(eventHurt!=null)
                eventHurt();
            }
            if(other.tag=="golpe3"&&!cubrir){
                this.GetComponent<Animator>().SetTrigger("pillar");
                vida -= 4;
                if(vida<0){
                    vida=0;
                }
                if(eventHurt!=null)
                eventHurt();
            }
            if(other.tag=="golpe4"&&!cubrir){
                this.GetComponent<Animator>().SetTrigger("pillar");
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 10);
            vida -= 3;
                if(vida<0){
                    vida=0;
                }
                if(eventHurt!=null)
                eventHurt();
            }
            if(other.tag=="DisparoL"&&!cubrir){
                this.GetComponent<Animator>().SetTrigger("pillar");
                vida -= 6;
                if(vida<0){
                    vida=0;
                }
                if(eventHurt!=null)
                eventHurt();
            }

        }
}
