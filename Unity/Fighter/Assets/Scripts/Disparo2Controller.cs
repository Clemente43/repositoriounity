﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparo2Controller : MonoBehaviour
{
    public GameObject Law;

    // Start is called before the first frame update
    void Start()
    {
        Law = GameObject.Find("Law");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    //mira si el proyectil ha chocado
    void OnTriggerEnter2D(Collider2D colision)
    {
        if (colision.name == "Zoro")
        {

            Law.gameObject.GetComponent<Player2Controller>().nDisparos--;
            Law.gameObject.GetComponent<Player2Controller>().espadaPatras = false;
            Law.gameObject.GetComponent<Player2Controller>().espadaLanzada = false;

            Destroy(this.gameObject);
        }

        if(colision.tag=="suelo"&&Law.GetComponent<Player2Controller>().espadaPatras){
            
            Law.gameObject.GetComponent<Player2Controller>().nDisparos--;
            Law.gameObject.GetComponent<Player2Controller>().espadaPatras=false;
            Law.gameObject.GetComponent<Player2Controller>().espadaLanzada = false;

            Destroy(this.gameObject);
        }
    }
}
