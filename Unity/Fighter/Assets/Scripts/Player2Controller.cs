﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player2Controller : MonoBehaviour
{
    //Bools animator
    public bool abajo = false;
    public bool derecha = false;
    public bool izquierda = false;
    public bool cubrir = false;
    public bool parriba = false;
    public bool pabajo = false;
    public bool combo1 = false;
    public bool combo2 = false;
    
    //velocidad del personaje
    public float vel;
    public int salto;
    public bool golpe;

    //enemigo
    public GameObject enemigo;
    //vida del personaje
    public int vida=100;
    //delegado y evento para recibir daño
    public delegate void hurt();
    public event hurt eventHurt;
    //disparo
    public GameObject disparo;
    //disparo creado
    private GameObject newDisparo;
    //numero de disparos en escena
    public int nDisparos=0;
    //indica si el enemigo esta en la derecha
    public bool enemigoDer;
    //indica si el enemigo esta en la izquierda
    public bool enemigoIzq;
    //indica si el enemigoha girado
    public bool haGirado;
    //indica si el proyectil se ha lanzado
    public bool espadaLanzada=false;
    //indica si el proyectil ha ido hacia atras
    public bool espadaPatras=false;
    //corutina
    Coroutine lastcoroutine = null;
    //estado del combo
    public int status = 0;
    //indica si se puede hacer el combo
    public bool puedeCombo = true;
    //indica si esta colisionando
    public bool colisionado=true; 


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Bools animator
        this.GetComponent<Animator>().SetBool("derecha", derecha);
        this.GetComponent<Animator>().SetBool("izquierda", izquierda);
        this.GetComponent<Animator>().SetBool("abajo", abajo);
        this.GetComponent<Animator>().SetBool("cubrir", cubrir);
        this.GetComponent<Animator>().SetBool("parriba", parriba);
        this.GetComponent<Animator>().SetBool("pabajo", pabajo);

        
        if(this.GetComponent<PolygonCollider2D>().enabled){
            Destroy(this.GetComponent<PolygonCollider2D>());
        }
        this.gameObject.AddComponent<PolygonCollider2D>();
        this.GetComponent<PolygonCollider2D>().isTrigger=true;

        //Flip Personaje
        if(this.GetComponent<Transform>().position.x < enemigo.gameObject.GetComponent<Transform>().position.x&& !haGirado){
            enemigoDer = false;
            enemigoIzq = true;
            this.GetComponent<Transform>().Rotate(0, 180, 0);
            haGirado = true;

        }
        else if(this.GetComponent<Transform>().position.x > enemigo.gameObject.GetComponent<Transform>().position.x&& haGirado){
            enemigoIzq = false;
            enemigoDer = true;
            this.GetComponent<Transform>().Rotate(0, 180, 0);
            haGirado = false;

        }
        //Ir hacia la derecha    
        if (Input.GetKey(KeyCode.RightArrow))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
            derecha = true;

        }
        //Ir hacia la izquierda
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
            izquierda = true;
        }
        //Quedarse quieto
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
            izquierda = false;
            derecha = false;
            abajo = false;

        }
        //Agacharse
        if (Input.GetKey(KeyCode.DownArrow))
        {
            abajo = true;

        }
        //Salto
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            this.GetComponent<Rigidbody2D>().AddForce(Vector2.up * salto, ForceMode2D.Impulse);
            parriba = true;
        }
        else
        {
            abajo = false;
        }
        //Comprovar si cae o si sube
        if (this.GetComponent<Rigidbody2D>().velocity.y < 0)
        {
            pabajo = true;
            parriba = false;
        }
        else
        {
            pabajo = false;
            
        }
        //Cubrirse
        if (Input.GetKey(KeyCode.Keypad3))
        {
            cubrir=true;

        }
        else
        {
            cubrir = false;
            
        }
        //Llamada a la instanciacion del proyectil
        if (Input.GetKeyDown(KeyCode.Keypad0) &&nDisparos==0)
        {
            nDisparos++;
            Disparo();
        }
        //Si le das a la tecla del proyectil y ya esta creado vuelve hacia atras
        if(Input.GetKeyDown(KeyCode.Keypad0) &&espadaLanzada&&!espadaPatras){
            if (enemigoDer)
            {
                newDisparo.GetComponent<Rigidbody2D>().velocity = new Vector2(10, 0);
            }
            else if (enemigoIzq)
            {
                newDisparo.GetComponent<Rigidbody2D>().velocity = new Vector2(-10, 0);
            }
            espadaLanzada =false;
            espadaPatras=true;
        }

        //Pegar Combo1
        if (Input.GetKeyDown(KeyCode.Keypad2))
        {
            switch (status)
            {
                case 0:
                    //this.GetComponent<Animator>().SetTrigger("combo1");
                    this.GetComponent<Animator>().Play("golpe1");
                    status = 1;
                    if (lastcoroutine != null)
                        StopCoroutine(lastcoroutine);
                    lastcoroutine = StartCoroutine(Combo1());


                    break;
                case 1:

                    if (puedeCombo)
                    {
                        //this.GetComponent<Animator>().SetTrigger("combo12");
                        this.GetComponent<Animator>().Play("golpe2");
                        status = 2;
                        StopCoroutine(lastcoroutine);
                        lastcoroutine = StartCoroutine(Combo1());

                    }
                    else
                    {
                        status = 0;
                    }

                    break;
                case 2:
                    if (puedeCombo)
                    {
                        //this.GetComponent<Animator>().SetTrigger("combo13");
                        this.GetComponent<Animator>().Play("golpe3");
                        enemigo.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 17);
                        this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 15);
                        status = 3;
                        StopCoroutine(lastcoroutine);
                        lastcoroutine = StartCoroutine(Combo1());
                    }
                    else
                    {
                        status = 0;
                    }

                    break;
                case 3:

                    if (puedeCombo)
                    {
                        //this.GetComponent<Animator>().SetTrigger("combo14");
                        this.GetComponent<Animator>().Play("golpe4");     
                        StopCoroutine(lastcoroutine);
                        lastcoroutine = StartCoroutine(Combo1());
                        status = 0;
                    }
                    else
                    {
                        status = 0;
                    }

                    break;

            }

        }
        //vistoria player2
        if (vida == 0)
        {
            Application.LoadLevel("Player2_Win");
        }

    }
    //Instanciacion del proyectil
    private void Disparo()
    {
            newDisparo = Instantiate(disparo);
            if(enemigoDer){
            newDisparo.transform.position = new Vector2(this.transform.position.x-3, this.transform.position.y);

            }else if(enemigoIzq){
            newDisparo.transform.position = new Vector2(this.transform.position.x+3, this.transform.position.y);

            }
    }
    //colisiones con los proyectiles y golpes
    private void OnTriggerEnter2D(Collider2D other) {
            if(other.tag=="DisparoL"){
                if(Input.GetKey(KeyCode.Keypad0)){
                if (enemigoDer)
                {
                    newDisparo.GetComponent<Rigidbody2D>().velocity = new Vector2(-5, 10);

                }
                else if(enemigoIzq)
                {
                    newDisparo.GetComponent<Rigidbody2D>().velocity = new Vector2(5, 10);
                }
                espadaLanzada =true;
                }
            }
            if(other.tag=="espadazo1"&&!cubrir){
                this.GetComponent<Animator>().SetTrigger("pillar");
                vida -=1;
                if(vida<0){
                    vida=0;
                }
                if(eventHurt!=null)
                eventHurt();
            }
            if(other.tag=="espadazo2"&&!cubrir){
                this.GetComponent<Animator>().SetTrigger("pillar");
                vida -= 2;
                if(vida<0){
                    vida=0;
                }
                if(eventHurt!=null)
                eventHurt();
            }
            if(other.tag=="espadazo3"&&!cubrir){
                this.GetComponent<Animator>().SetTrigger("pillar");
                vida -= 3;
                if(vida<0){
                    vida=0;
                }
                if(eventHurt!=null)
                eventHurt();
            }
            if(other.tag=="espadazo4"&&!cubrir){
            if (enemigoDer)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-10, 0);
            }
            else if (enemigoIzq)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(10, 0);
            }
            this.GetComponent<Animator>().SetTrigger("pillar");
                vida -=2;
                if(vida<0){
                    vida=0;
                }
                if(eventHurt!=null)
                eventHurt();
            }
            if(other.tag=="disparoZ"&&!cubrir){
                this.GetComponent<Animator>().SetTrigger("pillar");
            if (enemigoDer)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-5, 0);
            }
            else if (enemigoIzq)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(5, 0);
            }
            vida -= 5;
                if(vida<0){
                    vida=0;
                }
                if(eventHurt!=null)
                eventHurt();
            }

        }
    //Tiempo minimo y maximo para hacer el combo de golpes
    IEnumerator Combo1()
    {
        // ESPERA durante un segundo. el resto del codigo sigue ejecutandose.
        yield return new WaitForSeconds(0.3f);
        puedeCombo = true;
        yield return new WaitForSeconds(1);
        puedeCombo = false;
    }

}
