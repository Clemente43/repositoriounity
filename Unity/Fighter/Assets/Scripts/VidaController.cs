﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VidaController : MonoBehaviour
{
    public GameObject Zoro;
    //barra de vida
    private Transform barra;
    private int vidaActual;
    private int vidaInicial = 100;


    //suscripcion al evento la funcion de restar vida
    void Start()
    {
        Zoro = GameObject.Find("Zoro");
        barra = transform.Find("Verde");
        vidaInicial = Zoro.gameObject.GetComponent<PlayerController>().vida;
        Zoro.GetComponent<PlayerController>().eventHurt += restarVida;

    }


    // Update is called once per frame
    void Update()
    {
    }
    //funcion de restar vida
    void restarVida()
    {

        vidaActual = Zoro.gameObject.GetComponent<PlayerController>().vida;
        float vidaEnemigo = ((vidaActual * 100) / vidaInicial) * 0.01f;
        barra.localScale = new Vector3(vidaEnemigo, 1f);

    }
}