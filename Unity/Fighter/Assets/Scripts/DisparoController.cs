﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisparoController : MonoBehaviour
{
    //indica si el proyectil ha chocado
    public bool haChocau=false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    //mira si el proyectil ha chocado
    void OnTriggerEnter2D(Collider2D colision)
    {
        if(colision.name=="Law"||colision.tag=="suelo"){
            Destroy(this.gameObject);
            haChocau=true;
        }
    }

}
