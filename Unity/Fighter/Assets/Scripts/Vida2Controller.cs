﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vida2Controller : MonoBehaviour
{

    public GameObject Law;
    //barra de vida
    private Transform barra;
    private int vidaActual;
    private int vidaInicial=100;


    //suscripcion al evento la funcion de restar vida
    void Start()
    {
        Law = GameObject.Find("Law");
        barra = transform.Find("Verde");
        vidaInicial = Law.gameObject.GetComponent<Player2Controller>().vida;
        Law.GetComponent<Player2Controller>().eventHurt += restarVida;

    }


    // Update is called once per frame
    void Update()
    {
    }
    //funcion de restar vida
    void restarVida(){

        vidaActual = Law.gameObject.GetComponent<Player2Controller>().vida;
        float vidaEnemigo = ((vidaActual * 100) / vidaInicial)*0.01f;
        barra.localScale = new Vector3(vidaEnemigo, 1f);

    }
}
