﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour
{

    public GameObject enemigo;
    private Vector3 posEnemigo;
    private Vector3 axis = new Vector3(0, 0, 1);
    private float vel = 250f;

    void Start()
    {

    }
    void Update()
    {
        //Cogemos pos enemigo
        posEnemigo = new Vector3(enemigo.transform.position.x, enemigo.transform.position.y, enemigo.transform.position.z);
        //Hacemos que rote la bola de fuego con la posicion del enemigo, axis = eje sobre el que gira y por ultimo la velocidad
        this.transform.RotateAround(posEnemigo, axis, Time.deltaTime * vel);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Si colisiona con el enemigo le quita el ataque del boss/2 
        if (collision.CompareTag("Player"))
        {
            PlayerControler.vida -= enemigo.GetComponent<Enemigo>().ataque/2;
        }
    }
}
