﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : MonoBehaviour
{
    
    public PlayerControler playerControler;
    //Bool para que te siga el enemigo
    public static bool activado = false;
    //Bools direcciones
    public bool arriba = false;
    public bool abajo = false;
    public bool derecha = false;
    public bool izquierda = false;
    private bool diag = false;
    //Velocidades
    private float velY = 0;
    private float velX = 0;
    //Estadisticas
    public float vida = 4f;
    public float ataque = 0.5f;
    public float darXp = 5f;
    bool xp = true;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Si el player entra en el menu
        if (!PlayerControler.noMover)
        {

            //Si tiene 0 vidas muere y le da su experiencia al player
            if (vida <= 0)
            {
                if (xp)
                {
                    Debug.Log("AntesSuma:" + PlayerControler.xpActual);
                    PlayerControler.xpActual += this.darXp;
                    Debug.Log("DespuesSuma:" + PlayerControler.xpActual);
                    xp = false;
                }
                Destroy(this.gameObject);
            }
            //Bools animator
            this.GetComponent<Animator>().SetBool("Arriba", arriba);
            this.GetComponent<Animator>().SetBool("Abajo", abajo);
            this.GetComponent<Animator>().SetBool("Derecha", derecha);
            this.GetComponent<Animator>().SetBool("Izquierda", izquierda);


            if (activado)
            {
                //Coge la posicion del player y dependiendo de si se mueve en diagonal o no cambia la velocidad
                compararPos();
                if ((arriba && derecha || arriba && izquierda || abajo && derecha || abajo && izquierda) && !diag)
                {
                    velX *= 0.5f;
                    velY *= 0.5f;
                    diag = true;
                }
                else if (diag)
                {
                    diag = false;
                }
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(velX, velY);

            }
            else
            {
                //No se mueve
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                arriba = false;
                abajo = false;
                derecha = false;
                izquierda = false;
            }
        }
        //Si el player sale del menu
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        }
    }
    private void OnTriggerEnter2D(Collider2D other) {
        //Cuando el radio del trigger entra en contacto con el player le persigue
        if (other.CompareTag("Player")) {
            activado = true;
            this.playerControler = other.gameObject.GetComponent<PlayerControler>();
        }
    }
    private void OnTriggerExit2D(Collider2D other) {
        //Cuando el player se sale del radio del trigger deja de seguirle
        if (other.CompareTag("Player"))
        {
            activado = false;
        }
            
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Cuando colisiona con el player llama a la funcion KnockBack del player para que haga el retroceso y al player
        //se le resta el ataque del enemigo
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<PlayerControler>().KnockBack(this.arriba, this.abajo,
                this.izquierda, this.derecha);
            PlayerControler.vida -= this.ataque;
        }
    }

    //Mira la posicion del player y en funcion de esa posicion activa los bools de movimiento
    private void compararPos() {
        float distanciaX = playerControler.GetComponent<Transform>().position.x - this.GetComponent<Transform>().position.x;
        float distanciaY = playerControler.GetComponent<Transform>().position.y - this.GetComponent<Transform>().position.y;
        if (Mathf.Abs(distanciaX) > Mathf.Abs(distanciaY))
        {
            if (distanciaX > 0.1f)
            {
                derecha = true;
                izquierda = false;
                abajo = false;
                arriba = false;
            }
            else
            {
                derecha = false;
                izquierda = true;
                abajo = false;
                arriba = false;
            }
        }else if (Mathf.Abs(distanciaX) < Mathf.Abs(distanciaY))
        {
            if (distanciaY > 0.1f)
            {
                derecha = false;
                izquierda = false;
                abajo = false;
                arriba = true;
            }
            else
            {
                derecha = false;
                izquierda = false;
                abajo = true;
                arriba = false;
            }
        }
        if (distanciaX > 0.1f)
        {
            distanciaX = 2.5f;
        }
        else if (distanciaX < -0.1f)
        {
            distanciaX = -2.5f;
        }
        else
        {
            distanciaX = 0;
        }
        if (distanciaY > 0.1f)
        {
            distanciaY = 2.5f;
        }
        else if (distanciaY < -0.1f)
        {
            distanciaY = -2.5f;
        }
        else
        {
            distanciaY = 0;
        }

        velX = distanciaX;
        velY = distanciaY;

    }
    //Recibe los bools del player y hace el knockback en la direccion que le toque
    //Tambien le cambia el color y hace un Invoke a EnableMovement() para que vuelva a su estado normal
    public void KnockBack(bool arriba, bool abajo, bool izquierda, bool derecha)
    {
        if (arriba)
        {
            Debug.Log("Arriba");
            this.transform.position = new Vector2(this.GetComponent<Rigidbody2D>().position.x, this.GetComponent<Rigidbody2D>().position.y + 1f);
        }
        else if (abajo)
        {
             Debug.Log("Abajo");
            this.transform.position = new Vector2(this.GetComponent<Rigidbody2D>().position.x, this.GetComponent<Rigidbody2D>().position.y - 1f);
        }
        else if (izquierda)
        {
             Debug.Log("Izquierda");
            this.transform.position = new Vector2(this.GetComponent<Rigidbody2D>().position.x - 1f, this.GetComponent<Rigidbody2D>().position.y);
        }
        else if (derecha)
        {
             Debug.Log("Derecha");
            this.transform.position = new Vector2(this.GetComponent<Rigidbody2D>().position.x + 1f, this.GetComponent<Rigidbody2D>().position.y);
        }
        activado = false;
        Invoke("EnableMovement", 0.7f);
        Color color = new Color(255 / 255f, 75 / 255f, 0 / 255f);
        this.GetComponent<SpriteRenderer>().color = color;

    }
    //Vuelve el color por defecto del enemigo y el enemigo vuelve a seguir al player
    void EnableMovement()
    {
        this.GetComponent<SpriteRenderer>().color = Color.white;
        activado = true;
        this.playerControler = playerControler.gameObject.GetComponent<PlayerControler>();
    }

}
