﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PorNivel : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //Actualiza el % del nivel del player
        this.GetComponent<Text>().text = BarraHUD.nivelPor + "%";
    }
}
