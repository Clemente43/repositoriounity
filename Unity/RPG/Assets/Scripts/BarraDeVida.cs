﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarraDeVida : MonoBehaviour
{
    // Start is called before the first frame update
    public Enemigo enemigo;
    private Transform bar;
    private float vidaInicial;
    private float vidaActual;

    private void Start()
    {
        //Accedemos a la gameObject "Barra" y la vidaInicial=a la vida que tiene al principio el enemigo
        bar = transform.Find("Barra");
        vidaInicial = enemigo.gameObject.GetComponent<Enemigo>().vida;
    }

    // Update is called once per frame
    void Update()
    {
        //Vamos revisando la vida
        vidaActual = enemigo.gameObject.GetComponent<Enemigo>().vida;
        Invoke("PonerMedida", 2);
    }
    //Hacemos una regla de 3 para que la vida sea proporcional a la barra
    public void PonerMedida()
    {
        float vidaEnemigo = ((vidaActual * 100) / vidaInicial)*0.01f;
        bar.localScale = new Vector3(vidaEnemigo, 1f);
    }
}
