﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlechasDisparadas : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //Si la flecha colisiona con un enemigo le resta el ataque del player a la vida del enemigo y luego destruye la flecha 
        if (collision.gameObject.tag == "Enemigo")
        { 
                collision.gameObject.GetComponent<Enemigo>().vida -= PlayerControler.ataque;
                Destroy(this.gameObject);

        }
        //Si colisiona con un jarron llama a la funcion del jarron Objeto que genera un objeto y luego destruye el jarron y la flecha 
        else if(collision.gameObject.tag== "Jarron")
        {
            collision.gameObject.GetComponent<JarronControler>().Objeto();
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
        }
        Destroy(this.gameObject);

    }
}
