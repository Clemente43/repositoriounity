﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Tepear : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    //Cuando un objeto puerta colisiona con el player, mira como se llama ese objeto y carga una escena o otra, dependiendo de si la escena
    //tiene mas de un acceso hay que activar y desactivar bools para que luego el player aparezca en la poscion que le toque
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name == "links_0")
        {
            switch (this.gameObject.name)
            {
                case "PuertaCasaSalida":
                SceneManager.LoadScene("Mapa0");
                    PlayerControler.mapa0vuelta = false;
                    PlayerControler.tiendaVuelta = false;
                    PlayerControler.tiendaVuelta1 = false;
                    PlayerControler.tutorial = false;
                    PlayerControler.cueva = false;
                    break;

                case "Mazmorra":
                    SceneManager.LoadScene("Mazmorra0");
                    break;

                case "PuertaCasaEntrada":
                    SceneManager.LoadScene("CasaInicial");
                    break;
                case "Mazmorra0Volver":
                    PlayerControler.mapa0vuelta = true;
                    PlayerControler.tiendaVuelta = false;
                    PlayerControler.tiendaVuelta1 = false;
                    PlayerControler.tutorial = false;
                    PlayerControler.cueva = false;
                    SceneManager.LoadScene("Mapa0");
                    break;
                case "Mazmorra0To1":
                    PlayerControler.mazmorra1vuelta = false;
                    SceneManager.LoadScene("Mazmorra1");
                    break;
                case "Mazmorra0To2":
                    PlayerControler.mazmorra2vuelta = false;
                    SceneManager.LoadScene("Mazmorra2");
                    break;
                case "Mazmorra0To3":
                    SceneManager.LoadScene("Mazmorra3");
                    break;
                case "Mazmorra0To4":
                    PlayerControler.mazmorra41 = false;
                    SceneManager.LoadScene("Mazmorra4");
                    break;
                case "Mazmorra0To41":
                    PlayerControler.mazmorra41 = true;
                    SceneManager.LoadScene("Mazmorra4");
                    break;
                case "Mazmorra0To5":
                    SceneManager.LoadScene("Mazmorra5");
                    break;
                case "Mazmorra0To6":
                    PlayerControler.mazmorra6vuelta = false;
                    SceneManager.LoadScene("Mazmorra6");
                    break;
                case "Mazmorra1To0":
                    PlayerControler.mazmorra01vuelta = true;
                    PlayerControler.mazmorra02vuelta = false;
                    PlayerControler.mazmorra03vuelta = false;
                    PlayerControler.mazmorra04vuelta = false;
                    PlayerControler.mazmorra041vuelta = false;
                    PlayerControler.mazmorra05vuelta = false;
                    PlayerControler.mazmorra06vuelta = false;
                    SceneManager.LoadScene("Mazmorra0");
                    break;
                case "Mazmorra1To11":
                    SceneManager.LoadScene("Mazmorra11");
                    break;
                case "Mazmorra11To1":
                    PlayerControler.mazmorra1vuelta = true;
                    SceneManager.LoadScene("Mazmorra1");
                    break;
                case "Mazmorra2To0":
                    PlayerControler.mazmorra01vuelta = false;
                    PlayerControler.mazmorra02vuelta = true;
                    PlayerControler.mazmorra03vuelta = false;
                    PlayerControler.mazmorra04vuelta = false;
                    PlayerControler.mazmorra041vuelta = false;
                    PlayerControler.mazmorra05vuelta = false;
                    PlayerControler.mazmorra06vuelta = false;
                    SceneManager.LoadScene("Mazmorra0");
                    break;
                case "Mazmorra2To21":
                    SceneManager.LoadScene("Mazmorra21");
                    break;
                case "Mazmorra21To2":
                    PlayerControler.mazmorra2vuelta = true;
                    SceneManager.LoadScene("Mazmorra2");
                    break;
                case "Mazmorra3To0":
                    PlayerControler.mazmorra01vuelta = false;
                    PlayerControler.mazmorra02vuelta = false;
                    PlayerControler.mazmorra03vuelta = true;
                    PlayerControler.mazmorra04vuelta = false;
                    PlayerControler.mazmorra041vuelta = false;
                    PlayerControler.mazmorra05vuelta = false;
                    PlayerControler.mazmorra06vuelta = false;
                    SceneManager.LoadScene("Mazmorra0");
                    break;
                case "Mazmorra4To0":
                    PlayerControler.mazmorra01vuelta = false;
                    PlayerControler.mazmorra02vuelta = false;
                    PlayerControler.mazmorra03vuelta = false;
                    PlayerControler.mazmorra04vuelta = true;
                    PlayerControler.mazmorra041vuelta = false;
                    PlayerControler.mazmorra05vuelta = false;
                    PlayerControler.mazmorra06vuelta = false;
                    SceneManager.LoadScene("Mazmorra0");
                    break;
                case "Mazmorra41To0":
                    PlayerControler.mazmorra01vuelta = false;
                    PlayerControler.mazmorra02vuelta = false;
                    PlayerControler.mazmorra03vuelta = false;
                    PlayerControler.mazmorra04vuelta = false;
                    PlayerControler.mazmorra041vuelta = true;
                    PlayerControler.mazmorra05vuelta = false;
                    PlayerControler.mazmorra06vuelta = false;
                    SceneManager.LoadScene("Mazmorra0");
                    break;
                case "Mazmorra5To0":
                    PlayerControler.mazmorra01vuelta = false;
                    PlayerControler.mazmorra02vuelta = false;
                    PlayerControler.mazmorra03vuelta = false;
                    PlayerControler.mazmorra04vuelta = false;
                    PlayerControler.mazmorra041vuelta = false;
                    PlayerControler.mazmorra05vuelta = true;
                    PlayerControler.mazmorra06vuelta = false;
                    SceneManager.LoadScene("Mazmorra0");
                    break;
                case "Mazmorra6To0":
                    PlayerControler.mazmorra01vuelta = false;
                    PlayerControler.mazmorra02vuelta = false;
                    PlayerControler.mazmorra03vuelta = false;
                    PlayerControler.mazmorra04vuelta = false;
                    PlayerControler.mazmorra041vuelta = false;
                    PlayerControler.mazmorra05vuelta = false;
                    PlayerControler.mazmorra06vuelta = true;
                    SceneManager.LoadScene("Mazmorra0");
                    break;
                case "Mazmorra6To61":
                    PlayerControler.mazmorra61vuelta = false;
                    SceneManager.LoadScene("Mazmorra61");
                    break;
                case "Mazmorra61To6":
                    PlayerControler.mazmorra6vuelta = true;
                    SceneManager.LoadScene("Mazmorra6");
                    break;
                case "Mazmorra61To62":
                    SceneManager.LoadScene("Mazmorra62");
                    break;
                case "Mazmorra62To61":
                    PlayerControler.mazmorra61vuelta = true;
                    SceneManager.LoadScene("Mazmorra61");
                    break;
                case "TiendaSalida":
                    SceneManager.LoadScene("Mapa0");
                    PlayerControler.mapa0vuelta = false;
                    PlayerControler.tiendaVuelta = true;
                    PlayerControler.tiendaVuelta1 = false;
                    PlayerControler.tutorial = false;
                    break;
                case "TiendaSalida1":
                    SceneManager.LoadScene("Mapa0");
                    PlayerControler.mapa0vuelta = false;
                    PlayerControler.tiendaVuelta = false;
                    PlayerControler.tiendaVuelta1 = true;
                    PlayerControler.tutorial = false;
                    PlayerControler.cueva = false;
                    break;
                case "TiendaEntrada":
                    SceneManager.LoadScene("Tienda");
                    PlayerControler.tiendaEntrada1 = false;
                    break;
                case "TiendaEntrada1":
                    SceneManager.LoadScene("Tienda");
                    PlayerControler.tiendaEntrada1 = true;
                    break;
                case "TutorialSalida":
                    SceneManager.LoadScene("Mapa0");
                    PlayerControler.mapa0vuelta = false;
                    PlayerControler.tiendaVuelta = false;
                    PlayerControler.tiendaVuelta1 = false;
                    PlayerControler.tutorial = true;
                    PlayerControler.cueva = false;
                    break;
                case "EntradaTutorial":
                    if (!PlayerControler.tutorial1vez)
                    {
                        PlayerControler.tutorial1vez = true;
                    }
                    SceneManager.LoadScene("SalaTurorial");

                    break;
                case "Cueva":
                    SceneManager.LoadScene("Cueva");
                    break;
                case "CuevaSalida":
                    SceneManager.LoadScene("Mapa0");
                    PlayerControler.mapa0vuelta = false;
                    PlayerControler.tiendaVuelta = false;
                    PlayerControler.tiendaVuelta1 = false;
                    PlayerControler.tutorial = false;
                    PlayerControler.cueva = true;
                    break;
                default:
                
                    
                break;
            }
        }
    }
}
