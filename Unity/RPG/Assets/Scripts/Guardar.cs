﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Dynamic;
using UnityEngine.Experimental.UIElements;
using UnityEngine.UI;

public class Guardar : MonoBehaviour
{
    public PlayerControler playerControler = null;
 
    //Guarda las variables importantes en la ruta que indicamos
    public void Serializar(PlayerControler playerControler)
    {
        persistenciaPartida partida = new persistenciaPartida();
        
            partida.escena = playerControler.nombreMapa;
            partida.coordenadaX = playerControler.GetComponent<Transform>().position.x;
            partida.coordenadaY = playerControler.GetComponent<Transform>().position.y;
            partida.vida = PlayerControler.vida;
            partida.ataque = PlayerControler.ataque;
            partida.xpNivel = PlayerControler.xpNivel;
            partida.xpActual = PlayerControler.xpActual;
            partida.nivel = PlayerControler.nivel;
            partida.rupias = PlayerControler.rupias;
            partida.flechas = PlayerControler.flechas;
            partida.bombas = PlayerControler.bombas;

        

        string json = JsonUtility.ToJson(partida,true);
        string path = @".\Assets\Save\datosGuardado.json";
        System.IO.File.WriteAllText(path, json);
    }
   

}
[System.Serializable]
public class persistenciaPartida
{
    public string escena ;
    public float coordenadaX ;
    public float coordenadaY ;

    public float vida ;
    public float ataque ;
    public float xpNivel;
    public float xpActual ;
    public int nivel ;
    public int rupias ;
    public int flechas ;
    public int bombas;
    

    public static persistenciaPartida CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<persistenciaPartida>(jsonString);
    }
}



