﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjetoActual : MonoBehaviour
{
    public static int num = 0;
    public GameObject pocion;
    public GameObject pocionVacia;
    public GameObject bomba;
    public GameObject bombaVacia;
    public GameObject arco;
    public GameObject arcoVacio;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        CambiarObjeto();
        ComprobarObjeto();
    }
    //Puedes cambiar el objeto actual (arco, bomba o pocion) con la tecla "q" o la tecla "e"
    void CambiarObjeto()
    {
        if (Input.GetKeyDown("q"))
        {
            if (num == 0)
            {
                num = 2;
            }
            else 
            {
                num--;
            }
        }
        if (Input.GetKeyDown("e"))
        {
            if (num == 2)
            {
                num = 0;
            }
            else
            {
                num++;
            }
        }
    }
    //Comprueba que objeto es el que hay y dependiondo de de cuantos copias tienes del objeto sale un dibujo o otro
    //Por ejemplo, si no tienes flechas te saldra la imagen con un arco, pero si tienes flechas la imagen sera la de un arco con flechas
    void ComprobarObjeto()
    {
        if (num == 0)
        {
            if (PlayerControler.pociones > 0)
            {
                pocion.SetActive(true);
                pocionVacia.SetActive(false);
                bomba.SetActive(false);
                bombaVacia.SetActive(false);
                arco.SetActive(false);
                arcoVacio.SetActive(false);
            }
            else
            {
                pocion.SetActive(false);
                pocionVacia.SetActive(true);
                bomba.SetActive(false);
                bombaVacia.SetActive(false);
                arco.SetActive(false);
                arcoVacio.SetActive(false);
            }
        }
        else if (num == 1)
        {
            if (PlayerControler.bombas > 0)
            {
                pocion.SetActive(false);
                pocionVacia.SetActive(false);
                bomba.SetActive(true);
                bombaVacia.SetActive(false);
                arco.SetActive(false);
                arcoVacio.SetActive(false);
            }
            else
            {
                pocion.SetActive(false);
                pocionVacia.SetActive(false);
                bomba.SetActive(false);
                bombaVacia.SetActive(true);
                arco.SetActive(false);
                arcoVacio.SetActive(false);
            }
        }
        else if (num == 2)
        {
            if (PlayerControler.flechas > 0)
            {
                pocion.SetActive(false);
                pocionVacia.SetActive(false);
                bomba.SetActive(false);
                bombaVacia.SetActive(false);
                arco.SetActive(true);
                arcoVacio.SetActive(false);
            }
            else
            {
                pocion.SetActive(false);
                pocionVacia.SetActive(false);
                bomba.SetActive(false);
                bombaVacia.SetActive(false);
                arco.SetActive(false);
                arcoVacio.SetActive(true);
            }
        }
    }
}
