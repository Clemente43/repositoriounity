﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
    using UnityEngine.SceneManagement;

public class PlayerControler : MonoBehaviour
{
    // Start is called before the first frame update
    public float vel;
    public float velY;
   // public Scene[] scenes;
    private float velActual;
    private float velYActual;
    public bool arriba = false;
    public bool abajo = false;
    public bool derecha = false;
    public bool izquierda = false;
    public bool sprint = false;

    private bool diag = true;
    private bool espada = false;
    private int cooldown = 0;
    private int canShoot = 0;
    private Animator anim;

    private bool pocionTienda;
    private bool bombaTienda;
    private bool flechaTienda;

    public static bool menu=false;
    public GameObject menuIngame;

    //Valores Link
    public float vida = 8f;
    public float ataque = 1f;
    public static float xpNivel = 10f;
    public static float xpActual = 0;
    public static int nivel = 1;

    
    public static bool mapa0vuelta = false;
    public static bool mazmorra01vuelta = false;
    public static bool mazmorra02vuelta = false;
    public static bool mazmorra03vuelta = false;
    public static bool mazmorra04vuelta = false;
    public static bool mazmorra041vuelta = false;
    public static bool mazmorra05vuelta = false;
    public static bool mazmorra06vuelta = false;
    public static bool mazmorra1vuelta = false;
    public static bool mazmorra2vuelta = false;
    public static bool mazmorra41 = false;
    public static bool mazmorra6vuelta = false;
    public static bool mazmorra61vuelta = false;
    public static bool tiendaVuelta = false;
    public static bool tiendaVuelta1 = false;
    public static bool tiendaEntrada1 = false;

    //public static bool conseguirEspada = false;
    public static bool conseguirArco = true;
    public GameObject[] flechasArray;
    private GameObject newFlecha;

    //consumibles
    public static int rupias = 0;
    public static int flechas = 0;
    public static int bombas = 0;
    public static int pociones = 0;
    //delegado a consumibles
    // public Consumibles consumibles;

    static GameObject link = null;

    //OJO, ESTO ANTES NO ESTABA, SE INICIALIZABA como string nombreMapa = Application.loadedLevelName;
    public string nombreMapa=null;
    void Awake()
    {
        //Patró singleton
        if (link == null)
        {
            //crea el objecte en el primer moment
            link = this.gameObject;
            //per defecte els objectes es destrueixen al carregar una altra escena. 
            //D'aquesta manera no es destrueix al carretgarse
            DontDestroyOnLoad(link);
            /*vida = 8f;
            ataque = 1f;
            xpNivel = 10f;
            xpActual = 0;
            nivel = 1;*/
}
        else
        {
            //si no es el primer objecte creat (perque tornes a l'escena a on esc crea, es destrueix automàticament, d'aquesta forma no téns múltiples instàncies del mateix objecte (singleton)
            Destroy(this.gameObject);
        }
         nombreMapa = Application.loadedLevelName;
        //Debug.Log("Mapa: " + nombreMapa);
        switch (nombreMapa)
        {
            case "Mapa0":
                if (mapa0vuelta)
                {
                    link.transform.position = new Vector2(-13.4f, 1.42f);
                }
                else if (tiendaVuelta)
                {
                    link.transform.position = new Vector2(-69.11f, 54.78f);
                }
                else if (tiendaVuelta1)
                {
                    link.transform.position = new Vector2(-65.21f, 54.78f);
                }
                else
                {
                    link.transform.position = new Vector2(3.49f, -2.23f);
                }
                break;
            case "Mazmorra0":
                if (mazmorra01vuelta)
                {
                    link.transform.position = new Vector2(-24.45502f, 4.15f);
                }
                else if (mazmorra02vuelta)
                {
                    link.transform.position = new Vector2(-22.4909f, 32.04959f);
                }
                else if (mazmorra03vuelta)
                {
                    link.transform.position = new Vector2(-20.08911f, 34.5996f);
                }
                else if (mazmorra04vuelta)
                {
                    link.transform.position = new Vector2(-9.153177f, 34.67232f);
                }
                else if (mazmorra041vuelta)
                {
                    link.transform.position = new Vector2(8.907534f, 34.88992f);
                }
                else if (mazmorra05vuelta)
                {
                    link.transform.position = new Vector2(17.84668f, 34.71717f);
                }
                else if (mazmorra06vuelta)
                {
                    link.transform.position = new Vector2(22.88f, 4.42f);
                }
                break;
            case "Mazmorra1":
                if (mazmorra1vuelta)
                {
                    link.transform.position = new Vector2(-0.32f, 6.6f);
                }
                else
                {
                    link.transform.position = new Vector2(8.6f, 0.76f);
                }
                break;
            case "Mazmorra2":
                if (mazmorra2vuelta)
                {
                    link.transform.position = new Vector2(-0.02f, 6.55f);
                }
                else
                {
                    link.transform.position = new Vector2(8.5f, 0.46f);
                }
                break;
            case "Mazmorra4":
                if (mazmorra41)
                {
                    link.transform.position = new Vector2(8.88f, -5.92f);
                }
                else
                {
                    link.transform.position = new Vector2(-9.07f, -5.8f);
                }
                break;
            case "Mazmorra6":
                if (mazmorra6vuelta)
                {
                    link.transform.position = new Vector2(0.01f, 6.66f);
                }
                else
                {
                    link.transform.position = new Vector2(-8.394996f, 0.4650035f);
                }
                break;
            case "Mazmorra61":
                if (mazmorra61vuelta)
                {
                    link.transform.position = new Vector2(-0.06f, 16.52f);
                }
                else
                {
                    link.transform.position = new Vector2(-0.09f, -15.84f);
                }
                break;
            case "Tienda":
                if (tiendaEntrada1)
                {
                    link.transform.position = new Vector2(5.19f, -2.88f);
                }
                else
                {
                    link.transform.position = new Vector2(-4.71f, -2.88f);
                }


                break;
            default:

                break;
        }


        /*playerControler.GetComponent<PlayerControler>().onRupiasCambio += ModificarRupias;*/


    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        PlayerPrefs.SetFloat("VidaLink", vida);
        PlayerPrefs.SetFloat("XpNivel", xpNivel);
        PlayerPrefs.SetFloat("XpActual", xpActual);
        //PlayerPrefs.SetInt("Nivel", nivel);
        this.GetComponent<Animator>().SetBool("Arriba", arriba);
        this.GetComponent<Animator>().SetBool("Abajo", abajo);
        this.GetComponent<Animator>().SetBool("Derecha", derecha);
        this.GetComponent<Animator>().SetBool("Izquierda", izquierda);
        this.GetComponent<Animator>().SetBool("Espada", espada);
        /*if (this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("LinkDerecha") || this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("IdleDerecha"))
        {
            Debug.Log("Derecha");
        }
        else if (this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("LinkIzquierda") || this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("IdleIzquierda"))
        {
            Debug.Log("Izquierda");
        }
        else if (this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("LinkAbajo") || this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("IdleAbajo"))
        {
            Debug.Log("Abajo");
        }
        else if (this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("LinkArriba") || this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("IdleArriba"))
        {
            Debug.Log("Arriba");
        }*/
        if (Input.GetKey("space"))
        {
            if (ObjetoActual.num == 2)
            {
                Disparo();
            }
            
        }
        if (this.vida <= 0)
        {
            SceneManager.LoadScene("MenuPrincipal");
            // Destroy(this.gameObject);
        }

        if (Input.GetKey("w"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, vel);
            arriba = true;
            abajo = false;
        }
        else if (Input.GetKey("s"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, -vel);
            arriba = false;
            abajo = true;
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);
            arriba = false;
            abajo = false;
}
        if (Input.GetKey("d"))
        {   
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
            derecha = true;
            izquierda = false;
        }
        else if(Input.GetKey("a"))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
            derecha = false;
            izquierda = true;
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
            derecha = false;
            izquierda = false;
        }
        if (Input.GetKey("left shift") && !BarraStamina.cero)
        {
            sprint = true;
            BarraStamina.gastar = true;
            vel = 10f;
            velY = 10f;
        }
        else
        {
            sprint = false;
            BarraStamina.gastar = false;
            vel = 5f;
            velY = 5f;
        }
        if (sprint)
        {
            if ((arriba && derecha || arriba && izquierda || abajo && derecha || abajo && izquierda) && diag)
            {
                vel = vel * 0.75f;
                velY = velY * 0.75f;
                diag = false;
            }
            else if (!diag)
            {
                vel = 10f;
                velY = 10f;
                diag = true;
            }
        }
        else { 
            if((arriba && derecha || arriba && izquierda || abajo && derecha || abajo && izquierda) && diag)
            {
                vel = vel * 0.75f;
                velY = velY * 0.75f;
                diag = false;
            }
            else if (!diag)
            {
                vel = 5f;
                velY = 5f;
                diag = true;
            }
        }

        //Espadazo
        if (Input.GetKey("p"))
        {

            if (cooldown == 15 && !espada)
            {
                espada = true;
                cooldown = 0;
            }
        }
        
        
        if (cooldown == 15 && espada)
            {
                espada = false;
                cooldown = 0;
            }


        if (Input.GetKey(KeyCode.Escape))
        {
            menu = true;    
            menuIngame.SetActive(true);
            
        }
        
        if (pocionTienda && Input.GetKeyDown("b"))
        {
            Debug.Log("Pocion");
        }
        else if (bombaTienda && Input.GetKeyDown("b"))
        {
            Debug.Log("Bomba");
        }
        else if (flechaTienda && Input.GetKeyDown("b"))
        {
            Debug.Log("Flecha");
        }

    }
    private void FixedUpdate()
    {
        if (cooldown<15)
        {
            cooldown++;
        }
        if (canShoot < 25)
        {
            canShoot++;
        }
        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name == "comprarPocion")
        {
            pocionTienda = false;
        }
        else if (collision.gameObject.name == "comprarBomba")
        {
            bombaTienda = false;
        }
        else if (collision.gameObject.name == "comprarFlecha")
        {
            flechaTienda = false;
        }

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Jarron")
        {
            if (espada == true)
            {
                collision.gameObject.GetComponent<JarronControler>().Objeto();
                Destroy(collision.gameObject);
            }

        }
        if (collision.gameObject.tag == "Enemigo")
        {
            if (espada == true)
            {
                Debug.Log(arriba);
                Debug.Log(derecha);
                Debug.Log(izquierda);
                Debug.Log(abajo);
                collision.gameObject.GetComponent<Enemigo>().KnockBack(this.arriba, this.abajo, this.izquierda, this.derecha);
                collision.gameObject.GetComponent<Enemigo>().vida -= this.ataque;
            }
        }
        /*if (onRupiasCambio != null)
        {
            
            rupias += onRupiasCambio();

        }*/
        if (collision.gameObject.name == "comprarPocion")
        {
            pocionTienda = true;

        }
        else if (collision.gameObject.name == "comprarBomba")
        {
            bombaTienda = true;

        }
        else if (collision.gameObject.name == "comprarFlecha")
        {
            flechaTienda = true;

        }


       
            
            
            //no borrar los comentarios
            // PlayerPrefs.SetInt("Rupias", PlayerPrefs.GetInt("Rupias") + ModificarRupias(collision));



            if (collision.CompareTag("Verde"))
            {
            Debug.Log("verde");
                rupias++;
            }
            else if (collision.CompareTag("Azul"))
            {
                rupias+= 5;
            Debug.Log("azul");
            }
            else if (collision.CompareTag("Roja"))
            {
            Debug.Log("roja");
            rupias += 20;
            }
        
                if (rupias > 999)
                {
                    rupias = 999;
                }
                
                
            

            if (collision.CompareTag("Flecha"))
            {
                //PlayerPrefs.SetInt("Flechas", PlayerPrefs.GetInt("Flechas") + 1);
                flechas++;
                if (flechas > 30)
                {
                    flechas = 30;
                }
                
            }else
            if (collision.CompareTag("Bomba"))
            {
                // PlayerPrefs.SetInt("Bombas", PlayerPrefs.GetInt("Bombas") + 1);
                bombas++;
            Debug.Log("Bomba");
                if (bombas > 10)
                {
                    bombas = 10;
                }
               
            }else 
            if(collision.CompareTag("Corazon"))
             {
            vida += 1f;
             }
            // mirar a ver si se puede hacer el delegado para la vida
        

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemigo")
        {
            KnockBack(collision.gameObject.GetComponent<Enemigo>().arriba, collision.gameObject.GetComponent<Enemigo>().abajo,
                collision.gameObject.GetComponent<Enemigo>().izquierda, collision.gameObject.GetComponent<Enemigo>().derecha);
            this.vida -= collision.gameObject.GetComponent<Enemigo>().ataque;

        }
        
    }
    public void KnockBack(bool arribaE, bool abajoE, bool izquierdaE, bool derechaE)
    {
        if (arribaE)
        {
            this.transform.position = new Vector2(this.GetComponent<Rigidbody2D>().position.x, this.GetComponent<Rigidbody2D>().position.y + 1f);
        }
        else if (abajoE)
        {
            this.transform.position = new Vector2(this.GetComponent<Rigidbody2D>().position.x, this.GetComponent<Rigidbody2D>().position.y - 1f);
        }
        else if (izquierdaE)
        {
            this.transform.position = new Vector2(this.GetComponent<Rigidbody2D>().position.x - 1f, this.GetComponent<Rigidbody2D>().position.y);
        }
        else if (derechaE)
        {
            this.transform.position = new Vector2(this.GetComponent<Rigidbody2D>().position.x + 1f, this.GetComponent<Rigidbody2D>().position.y);
        }
        Invoke("EnableMovement", 0.7f);
        Color color = new Color(255 / 255f, 75 / 255f, 0 / 255f);
        this.GetComponent<SpriteRenderer>().color = color;

    }
    void EnableMovement()
    {
        this.GetComponent<SpriteRenderer>().color = Color.white;
    }

    public void Disparo()
    {
        if (canShoot == 25 && flechas > 0 && (this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("LinkArriba") || this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("IdleArriba")))
        {
            newFlecha = Instantiate(flechasArray[0]);
            newFlecha.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 20);
            //mueve la posicion a la posicion del player
            newFlecha.transform.position = new Vector2(this.transform.position.x, this.transform.position.y + 1f);
            canShoot = 0;
        }
        else if (canShoot == 25 && flechas > 0 && (this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("LinkAbajo") || this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("IdleAbajo")))
        {
            newFlecha = Instantiate(flechasArray[1]);
            newFlecha.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -20);
            //mueve la posicion a la posicion del player
            newFlecha.transform.position = new Vector2(this.transform.position.x, this.transform.position.y - 1f);
            canShoot = 0;
        }
        else if (canShoot == 25 && flechas > 0 && (this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("LinkDerecha") || this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("IdleDerecha")))
        {
            newFlecha = Instantiate(flechasArray[2]);
            newFlecha.GetComponent<Rigidbody2D>().velocity = new Vector2(+20, 0);
            //mueve la posicion a la posicion del player
            newFlecha.transform.position = new Vector2(this.transform.position.x + 1f, this.transform.position.y);
            canShoot = 0;
        }
        else if (canShoot == 25 && flechas > 0 && (this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("LinkIzquierda") || this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("IdleIzquierda")))
        {
            newFlecha = Instantiate(flechasArray[3]);
            newFlecha.GetComponent<Rigidbody2D>().velocity = new Vector2(-20, 0);
            //mueve la posicion a la posicion del player
            newFlecha.transform.position = new Vector2(this.transform.position.x - 1f, this.transform.position.y);
            canShoot = 0;
        }
    }

    /* private int ModificarRupias(Collider2D collision)
     {

         if (collision.gameObject.name == "Verde")
         {
             return 1;
         }
         else if (collision.gameObject.name == "Azul")
         {
             return 5;
         }
         else if (collision.gameObject.name == "Roja")
         {

             return 20;
         }
         else {
             Debug.Log("algo va mal");
             return 0;
         }


     }*/
}
