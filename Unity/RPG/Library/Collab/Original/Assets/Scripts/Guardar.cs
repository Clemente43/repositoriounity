﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Dynamic;
using UnityEngine.Experimental.UIElements;
using UnityEngine.UI;

public class Guardar : MonoBehaviour
{
    public PlayerControler playerControler=null;
    //public Button guardar;


    // Start is called before the first frame update
    //FALTA NIVEL

    public void Serializar(PlayerControler playerControler)
    {
        var partida = new persistenciaPartida
        {
            escena = playerControler.nombreMapa,
            coordenadaX = playerControler.GetComponent<Transform>().position.x,
            coordenadaY = playerControler.GetComponent<Transform>().position.y,
            vida = PlayerControler.vida,
            ataque = PlayerControler.ataque,
            xpNivel = PlayerControler.xpNivel,
            xpActual = PlayerControler.xpActual,
            nivel = PlayerControler.nivel,
            rupias = PlayerControler.rupias,
            flechas = PlayerControler.flechas,
            bombas = PlayerControler.bombas

        };

        string json = JsonUtility.ToJson(partida);
        string path = @".\Assets\Save\datosGuardado.json";
        System.IO.File.WriteAllText(path, json);
        // System.IO.File.ReadAllText(@".\Save\datosGuardado.json");
        /*Button btn1 = guardar.GetComponent<Button>();
        guardar.GetComponent<Text>().text = "Guardar Partida";
        guardar.onClick.AddListener(TaskOnClick);*/
    }

}
[System.Serializable]
public class persistenciaPartida
{
    public string escena { get; set; } 
    public float coordenadaX { get; set; }
    public float coordenadaY { get; set; }

    public float vida { get; set; }
    public float ataque { get; set; }
    public float xpNivel { get; set; }
    public float xpActual { get; set; }
    public int nivel { get; set; }
    public int rupias { get; set; }
    public int flechas { get; set; }
    public int bombas { get; set; }
    

    public static persistenciaPartida CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<persistenciaPartida>(jsonString);
    }
    

    
}



