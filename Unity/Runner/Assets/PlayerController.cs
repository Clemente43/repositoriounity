﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //fuerza que ejerces al player para que salte
    public int salto;
    //velocidad del player
    public float vel;
    //guarda la velocidad que hay en ese momento
    private float actualVel;
    //se activa cuando quieres guardar la velocidad que hay en ese momento
    private bool velActual = true;
    //si esta en true el player puede saltar y si esta en false no
    private bool puedeSaltar = true;
    //si esta en true el player puede cambiar la gravedad y si esta en false no
    private bool puedeGirar = true;
    //si esta en true el player puede hacer un sprint y si esta en false no
    private bool puedeSprint = true;
    //tiempo de espera que el player esta quieto haciendo la animacion de sprint
    public int espera = 150;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(vel);
        morirse();
        //activar el cambio de animacion mediante el animator dependiendo de la variable puedeSprint. Si esta en true
        //esta la animacion de correr si esta en false se cambia por la de Sprint
        this.GetComponent<Animator>().SetBool("chidori", puedeSprint);
        //velocidad constante del player hacia la derecha
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(vel, this.GetComponent<Rigidbody2D>().velocity.y);
        //funcion de saltar del player cuando se pulsa la tecla "w"
        if (Input.GetKey("w"))
        {
            if (puedeSaltar)
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, salto));
                puedeSaltar = false;
            }
            
   
        }
        //funcion de cambiar la gravedad cuando se pulsa la tecla "space"
        if (Input.GetKey("space")){
            if (puedeGirar)
            {
                salto = -salto;
                this.GetComponent<Transform>().Rotate(180,0,0);
                this.GetComponent<Rigidbody2D>().gravityScale = -this.GetComponent<Rigidbody2D>().gravityScale;
                this.GetComponent<Rigidbody2D>().gravityScale*=10;
                puedeGirar = false;
            }
        }
        //cuando pulsas la d se para el player para hacer la animacion especial que a los 150 iteraciones vuelve a la
        //velocidad normal del jugador y si choca contra la pared con tag "Destruir" se destruye la pared.
        if (Input.GetKeyDown("d"))
        {
            if (puedeSprint)
            {
                puedeSprint = false;
                puedeSaltar = false;
                puedeGirar = false;

            }
        }
        if (espera <= 0&&!puedeSprint)
        {
            this.vel = actualVel+10;
            velActual = true;

        }
        if (!puedeSprint && espera > 0)
        {
            if (velActual)
            {

                actualVel = vel;
                velActual = false;
                this.vel = 0;

            }

            espera--;
        }

    }
    //detecta la colision del player con el suelo y si toca con el suelo se reactivan los booleanos de cambiar la gravedad
    //y saltar para que el player pueda volver a hacerlo
    private void OnCollisionEnter2D(Collision2D colision)
    {
        if (colision.gameObject.CompareTag("Suelo"))
        {
            if(!puedeGirar){
            this.GetComponent<Rigidbody2D>().gravityScale /= 10;
            }
            puedeSaltar = true;
            puedeGirar=true;
        }

    }
    //detecta el objeto con el que esta colisionando el player. Si colisiona con un enemigo el player muere. Si colisiona
    //con un muro destruible y no esta activado el sprint tambien muere si el sprint esta activado el muro se destruye
    void OnTriggerEnter2D(Collider2D colision)
    {
        Debug.Log(colision.tag);
        if (colision.tag == "Enemigo")
        {
            Destroy(this.gameObject);
        }else if (colision.tag == "Destruir"&&puedeSprint==false)
        {
            Destroy(colision.gameObject);
            this.vel = actualVel;
            puedeSprint = true;
            espera = 150;
            puedeSaltar = true;
            puedeGirar = true;
            this.GetComponent<Rigidbody2D>().gravityScale=1;
        }
        else if (colision.tag == "Destruir")
        {
            Destroy(this.gameObject);
        }
    }

    //mueres cuando empiexas a subir hacia arriba sin parar
    private void morirse()
    {
        if (this.GetComponent<Transform>().position.y>15)
        {
            Destroy(this.gameObject);
        }
    }

}
