﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparo : MonoBehaviour
{
 
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    
    }
    //detecta el objeto con el que esta colisionando el proyectil y si es un muro destruible este se destruye junto al
    //proyectil pero si es un enemigo solo se destruye el proyectil
     void OnTriggerEnter2D(Collider2D colision)
    {
        Debug.Log(colision.tag);
        if (colision.tag == "Destruir")
        {
            Destroy(this.gameObject);
            Destroy(colision.gameObject);
        }else if(colision.tag == "Enemigo"){
            Destroy(this.gameObject);
        }
    }
}
