﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rasengan : MonoBehaviour
{
    public int vel;
    public int salto;
    private bool puedeSaltar = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(-vel, this.GetComponent<Rigidbody2D>().velocity.y);
                if (puedeSaltar)
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, salto));
                puedeSaltar = false;
            }
            }    
    private void OnCollisionEnter2D(Collision2D colision)
    {
        if (colision.gameObject.CompareTag("Suelo"))
        {
            puedeSaltar = true;
        }

    }
}