﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameController : MonoBehaviour

{
    //imagen de fondo del juego
    public GameObject fondo;
    //score que sale por pantalla mostrando la puntuacion
    public Text textoScore;
    //objeto que disparara nuestro player
    public GameObject proyectil;
    //puntuacion del juego
    private int score = 0;
    //tiempo de espera entre disparo y disparo
    private int tiempo = 40;
    //nuestro personaje principal
    public PlayerController player;
    //camara del juego
    public Camera camera;
    //lista de prefabs
    public GameObject[] Prefabs;
    //puntero para la generacion de prefabs
    private float puntero;
    //distancia para la generacion de prefabs
    private float spawn = 30;
    //instanciacion del proyectil
    private GameObject newChidoraso;
    //iniciacion del score
    int initScore = 0;
    //velocidad de los proyectiles
    private float proyectilvel;
    // Start is called before the first frame update
    void Start()
    {
        //instanciacion del prefab en la posicion 1 cuando empieza el programa
        Instantiate(Prefabs[0]);
    }

    // Update is called once per frame
    void Update()
    {
        proyectilvel = player.vel + 20;
        dificultad();
        textoScore.text = "Score: "+ score;

        //el player lanza un proyectil cuando se pulsa la tecla p y ha acabado el tiempo de espera 
        if (Input.GetKey("p") && tiempo >= 40)
        {
            newChidoraso = Instantiate(proyectil);
            newChidoraso.transform.position = new Vector2(player.transform.position.x + 1, player.transform.position.y);
            newChidoraso.GetComponent<Rigidbody2D>().velocity = new Vector2(proyectilvel, 0);
            tiempo = 0;
        }
        else
        {
            tiempo++;
        }

        //seguimiento del fondo
        fondo.transform.position = new Vector3(
            fondo.transform.position.x,
            fondo.transform.position.y,
            fondo.transform.position.z);

        //seguimiento de la camara al personaje
        camera.transform.position = new Vector3(
            player.transform.position.x+10,
            player.transform.position.y + 2,
            camera.transform.position.z);


        //generacion de los prefabs automaticamente de forma aleatoria
        if (player != null && this.puntero < player.transform.position.x + spawn)
        {
            GameObject newBlock = Instantiate(Prefabs[Random.Range(1, Prefabs.Length)]);

            float size = newBlock.transform.GetChild(0).transform.localScale.x;

            newBlock.transform.position = new Vector2(puntero + size / 2, newBlock.transform.position.y);

            puntero += size;
        }

        score++;
    }
    //cada 1000 puntos la velocidad del juego aumenta en un 0.5 para subir la dificultad
    public void dificultad()
    {

            if (score==initScore+1000)
            {
                initScore = initScore+1000;
                player.vel += 0.5f;
            }
        
    }
}
